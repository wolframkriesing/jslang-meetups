# Generators

We had a bit of a hard time to find a good real world use case for generators.
In the end we went with a task that [Alexandr] suggested. We build a file transmitter.

The task is coding part of a file transmission controller, which gets
passed 1) the file and 2) an initial package size, which is the size of a chunk (or package)
to deliver on every call. Now the task is to implement the following:
* Show progress for data transmission
* When packet was delivered successfully 
  increase packet size 2 times
* When packet was not delivered successfully 
  decrease the packet size to 1
* When there is no connection stop transmission 
  and continue from the beginning, once the 
  connection occurs again


## How we ran the meetup

We organized the meetup on [meetup.com].

We started doing two generator katas on [es6katas.org] in order to warm up and get
a first handle on the generator functions syntax `function* () {}` and the `yield`
keyword. After that we tackled the task mentioned above.
 
[Alexandr]: https://www.meetup.com/de-DE/JavaScript-The-Language/members/193617708/
[the file]: ./file-transmitter.spec.js
[meetup.com]: https://www.meetup.com/de-DE/JavaScript-The-Language/events/242197330/