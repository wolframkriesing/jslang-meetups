# AsyncGenerators

Let's figure out what we can learn about it.

## Agenda

Onsite: 🍏 🍕 🥤 by **Alasco** https://www.alasco.de/  
Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are async generators
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [x] Choose next meetup topic
7) [x] Hangout ...

## Collect what we want to learn

- [x] is getting the next item async or the item itself
- [x] what happens with a value that has been consumed?
- [x] what's a diff between yield and return?
- [ ] how can we visualize an async generator?
- [ ] is a not-promise value wrapped into a promise, when yielding?
- [ ] is there map/reduce on a generator?
- [ ] how to work with for-loop and async generator?



## Topic for the next meetup

=> try-catch
   function and new Function
   







## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
