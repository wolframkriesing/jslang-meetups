# Map, Set, WeakMap

Let's figure out what we can learn about it.

## Agenda

Onsite: 🍏 🍕 🥤 by **CHECK24** https://www.check24.de/  
Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are Map, Set, WeakMap, WeakSet
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [ ] What happens when removing items, does it leave a "hole"?
- [ ] What complex keys can be used in Map (and not in object)?
- [ ] Garbage collection for Weak* ...
- [ ] What happens when you nest WeakSets?
- [ ] Removing from a Set???
- [x] set.add(1) is the same as set.add('1')??
- [x] How to check if an element is in a set?
- [x] What happens when we add 0 and -0?
- [ ] Can you chain add and remove?
- [ ] What about NaN, Infinity?


## Topic for the next meetup
 
   getter, setter
6  how to read the spec 
2  Promises
5  protoypes 
3  import, import()
1  top level await
1  classes
1  new.target property



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
