# `this` - #jslang 72nd Edition

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what is `this`
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn by writing a test

- [x] `function() {}` vs. `() => {}`
- [ ] nesting objects, and what does `this` refer to?
- [x] is `this` ever undefined?
- [ ] is `this` in the `prototype` of an object/instance?
- [x] `this.this`???
- [ ] assigning something to `this`?




## Topic for the next meetup

1  closures
2  context vs. environment (as defined in ECMAScript spec)
3  regular expressions
4  error handling, bubbling, etc.





## How we ran the meetup?

We organized the meetup on [meetup].

[meetup]: https://www.meetup.com/JavaScript-The-Language
