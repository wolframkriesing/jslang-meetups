import {strict as assert} from 'assert';

describe('modulo operator', () => {
  it('works on positive 7 % 2 = 1 - returns the remainder of the division', () => {
    assert.equal(7 % 2, 1);
  });
  it('returns the negative remainder for a division of a negative number', () => {
    assert.equal(-7 % 2, -1);
  });

  function modulo(x, y) {
    return x - (parseInt(x / y) * y);
  }

  it('returns a positive remainder for a division with a negative number', () => {
    assert.equal(7 % -2, 1);
    assert.equal(7 % -2, modulo(7, -2));
  });
  it('returns a negative remainder for a division with and of a negative number', () => {
    assert.equal(-7 % -2, -1);
  });
  it('returns a negative reminder with both negative numbers, using OUR modulo() function', () => {
    assert.equal(modulo(-7, -2), -1);
  });
});

describe('comma operator', () => {
  it('returns the right-most value', () => {
    const x = (true, false, 3);
    assert.equal(x, 3);
  });
  it('executes all expressions from left to right', () => {
    let x = 0;
    let y = 0;
    const z = (++x, y++, x+y);
    assert.equal(x, 1);
    assert.equal(y, 1);
    assert.equal(z, 2);
  });
  describe('execution stops when there is a throw', () => {
    const sut = () => {
      let x = 0;
      let y = 0;
      let z = 'without modification';
      function throwingFunction() {
        throw Error('nothing');
      }
      try {
        z = (++x, throwingFunction(), x+y);
      } catch {}
      return {x, y, z};
    }
    it('an assignment is stopped if there is a throw part of the comma-operators expression', () => {
      assert.equal(sut().z, 'without modification');
    });
    it('executing a comma-operator expression does execute everything before it throws', () => {
      assert.equal(sut().x, 1);
    });
  });
  it('an assignment is stopped if there is a throw part of the comma-operators expression', () => {
    let z = 'unmodified';
    function throwingFunction() {
      throw Error('nothing');
    }
    try {
      z = throwingFunction(), 'modified';
    } catch {}
    assert.equal(z, 'unmodified');
  });
  it('comma operator without surrounding () works NOT the same', () => {
    let x = 0;
    let y = 23;
    let z;
    z = ++x, y++;
    assert.equal(z, 1);
  });
});