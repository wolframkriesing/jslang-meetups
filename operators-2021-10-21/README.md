# Operators

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what operators are (e.g. 1+2, typeof X, built-in - unary operators, binary operators, ternary operator)
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn
- [x] modulo operator work with negative `a % b`
- [ ] `typeof` operator
- [ ] `delete` operator
- [ ] `x++` or `++x`
- [ ] `new` operator
- [ ] `+` vs `parseInt()`
- [x] comma operator
- [ ] how long can we make an operator list (jskatas.org)


## Topic for the next meetup
- 1  modulo operator
-    comma operator
- 4  delete operator
- 5  prototype
- 4  class expression


## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language

## References

- MDN page https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/
- This source code and other meetup's source https://codeberg.org/wolframkriesing/jslang-meetups