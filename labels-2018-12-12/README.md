# Labels

Again I got proven wrong. I thought this topic would not be enough for an
entire evening. So off we went, 2.5h experimenting with labels (and continue and break).
This was very exhausting.

We started by quickly looking at one [example of labels on MDN][mdn-example] and collecting 
ideas of what we want to learn about labels. Our list was the following:
* numbers as labels (to build some BASIC style goto labels)
* imitate GOTO
* jump into function
* how does context/arguments change when jumping to labels
* can we create dynamic labels
* can we imitate generators
* skip code
* jump between loops
* jump inside a statement, e.g. a `for`

See the code for the outcomes, it was definitely a lot of fun.
We also looked at the [spec].
 
[mdn-example]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/label
[spec]: https://tc39.github.io/ecma262/#sec-labelled-statements