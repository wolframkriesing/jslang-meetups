# flatMap, flat

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what flatMap, flat
4) [x] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [ ] Difference flatMap vs. flat()+map()?
- [x] How deep does flat() go?
- [x] Do they keep the order?
- [ ] Use cases
- [ ] How costly are they?
- [ ] What's the different between flatMap() and reduce()?
- [ ] Can we build reduce() using flatMap() or vice versa?
- [ ] Do they support the iterator interface?
- [ ] Can we do the same with recursion, why is it better/worse?
- [ ] What happens when you try to flatten an unflat-anable element?

## Topic for the next meetup

- 5 what is an object?
-   Symbols
- 5 types
- 1 flatMap(), flat()
- 4 prototype chain
- 5 temporal


## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
