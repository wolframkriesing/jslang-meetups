# Functions

To announce it, we gave it the title: `(function(){Function, function, () => {}, ...})()`.

## The plan

The plan for the meetup was to:

1. Collect the ways functions can be created
2. Collect properties of a function. (What makes a function a function?)
3. Write tests to learn, verify and describe the properties of the various ways a function can be generated. 
   And learn how those differ. (E.g. A context can not be bound on an arrow function.)
4. The challenge: Implement the behavior of arrow functions (`f=()=>{}`) using old-style functions (`f() {}`).

## How we ran the meetup

We organized the meetup on [meetup.com][meetup].

## Onsite

We started by introducing each other shortly.
Then we did 1. and collected all ways a function can be created. We came up with this:
* named function
* anonymous function
* arrow function
* function expression
* eval
* `Function`

Others we forgot to list
* getters and setters

Afterwards we collected properties of a function. This is what we came up with:
* bind
* scope
* closure
* param
* return
* `this`
* `arguments`
* `caller`
* `callee`
* strict mode
* `new`
* `call()`
* `apply()`
* methods (toString, valueOf, ...)
* properties

Then we started to write tests for the various features of function. We wrote the
tests for old-style functions and arrow functions. Actually we just got through
three items as you can see in the source code. We wrote tests for:
* `bind()`
* `arguments`
* `this`

We discussed a lot about various details of the features, we tried out a lot
of things and didn't get any further. Not even close to item 4. on the list above.
But it was great fun and I think we all learned a lot.

## Details

> An ArrowFunction does not define local bindings for arguments, super, this, or new.target. Any reference to arguments, super, this, or new.target within an ArrowFunction must resolve to a binding in a lexically enclosing environment.

from the [ES6 spec](http://www.ecma-international.org/ecma-262/6.0/#sec-arrow-function-definitions-runtime-semantics-evaluation)

[meetup]: https://www.meetup.com/de-DE/preview/JavaScript-The-Language/events/242446661