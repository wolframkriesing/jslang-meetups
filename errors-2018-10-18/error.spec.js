import assert from 'assert';

describe('types of errors', () => {
  it('access empty array return undefined', () => {
    assert.equal([][0], undefined);
  });
  it('access empty Int32Array return undefined', () => {
    assert.equal((new Int32Array(1))[3], undefined);
  });
  it('cause a RangeError, using `new Array(-1)`', () => {
    assert.throws(
      () => {new Array(-1)},
      RangeError
    );
  });
  it('cause a RangeError, using `new Array(Infitny)`', () => {
    assert.throws(
      () => {new Array(Infinity)},
      RangeError
    );
  });
  it('do NOT cause an Error, using `new Array({})`', () => {
    assert.deepEqual(new Array({}), [{}]);
  });
  it('cause a RangeError, using `new Array(NaN)`', () => {
    assert.throws(
      () => {new Array(NaN)},
      RangeError
    );
  });
  it('assign to the array key `Infinity` succeeds', () => {
    const a = [];
    a[Infinity] = 1;
    assert.deepEqual(1, a[Infinity])
  });
  it('assign to the array key `-Infinity` succeeds', () => {
    const a = [];
    a[-Infinity] = 1;
    assert.deepEqual(1, a[-Infinity])
  });
  it('an array with the key `Infinity`, has a length 0', () => {
    const a = [];
    a[-Infinity] = 1;
    assert.deepEqual(0, a.length)
  });

  it('`undefined()` causes a TypeError', () => {
    assert.throws(() => undefined(), TypeError);
  });
  it('accessing an undeclared object`s nested property, cause a TypeError', () => {
    const obj = {};
    assert.throws(() => obj.b.c, TypeError);
  });

  it('accessing an undeclared variable, cause a ReferenceError', () => {
    assert.throws(() => { const a = b; }, ReferenceError);
  });

  it('undefined is an object? (no, its undefined :))', () => {
    assert.equal(typeof undefined, 'undefined');
  });
  it('does undefined have a prototype?, No', () => {
    assert.throws(() => undefined.prototype, TypeError);
  });
  it('the message of accessing undefined`s prototype, is ???', () => {
    assert.throws(() => undefined.prototype,
      (e) => {
        return e.message === 'Cannot read property \'prototype\' of undefined';
      });
  });
  it('the message of accessing undefined`s prototype', () => {
    assert.throws(() => undefined.prototype,
      'TypeError: Cannot read property \'prototype\' of undefined'
    );
  });
  it('assigning to undefined, throws a TypeError', () => {
    assert.throws(() => undefined = 1, TypeError);
  });
  it('void 0 === undefined', () => {
    assert.equal(void 0, undefined);
  });
  it('type of typeof {}', () => {
    assert.equal(typeof typeof {}, 'string');
  });
  it('type of void 0', () => {
    assert.equal(typeof void 0, 'undefined');
  });

  it('when evaling invalid syntax, throws a SyntaxError', () => {
    assert.throws(() => eval('void'), SyntaxError);
  });
  it('when `Function(???)` invalid syntax, throws a SyntaxError', () => {
    assert.throws(() => Function('void'), SyntaxError);
  });
});

describe('props and methods of Error', () => {
  it('Object.keys of an error, return []', () => {
    assert.deepEqual(Object.keys(new TypeError('message')), []);
  });
  it('toString() and object', () => {
    assert.deepEqual(
      (new TypeError('message')).toString(), 'TypeError: message');
  });
  xit('JSON.stringify an error, contains "TypeError"', () => {
    // seems to pass in tddbin+Chrome, but not on node  
    assert.ok(
      /.*TypeError.*/.test(JSON.stringify(new TypeError('message')))
    );
  });
  it('passing a string to the constructor, results in `message`', () => {
    assert.deepEqual(new Error('msg').message, 'msg');
  });
});

describe('custom error classes', () => {
  it('`name` is "Error" even for user errors', () => {
    class MyError extends Error {}

    assert.deepEqual(new MyError().name, 'Error');
  });
  it('`name` has to be set explicitly', () => {
    class MyError extends Error {
      constructor() {
        super();
        this.name = 'MyError';
      }
    }

    assert.deepEqual(new MyError().name, 'MyError');
  });
});
