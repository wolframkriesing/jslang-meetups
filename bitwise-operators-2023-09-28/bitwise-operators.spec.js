import {strict as assert} from 'assert';

describe('Explore left shift bitwise operators', () => {
  it('left shift by 1 bit, makes a 2 out of a 1', () => {
    assert.equal(0b0000_0010, 0b0000_0001 << 1);
  });
  it('left shift by -1 bit, is the same as left shifting by 31', () => {
    assert.equal(
      0b0000_0000_0000_0000_0000_0000_0000_0001 << 31, 
      0b0000_0000_0000_0000_0000_0000_0000_0001 << -1
    );
  });
  it('left shifting by 3 is the same as multiplying by 2^3 (8)', () => {
    assert.equal(8, 1 << 3);
  });
  it('left shift 1 by 32, the number stays the same', () => {
    assert.equal(1 << 32, 1);
  });
  it('left shift 1n by 32n, the number is not 1n', () => {
    assert.notEqual(1n << 32n, 1n);
  });
  it('left shift 1n by 32n, results in 4294967296n', () => {
    assert.equal(1n << 32n, 4294967296n);
    assert.equal(1n << 32n, 0b1_0000_0000_0000_0000_0000_0000_0000_0000n);
  });
});
