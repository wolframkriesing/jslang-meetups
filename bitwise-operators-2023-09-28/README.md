# Bitwise operator

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what is the Bitwise Operators
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] how many bitwise SHIFT operators are there? - 3
- [x] since when is it in JS?
- [x] how do we use it?
- [x] how do left/right shift differ?
- [ ] any good use cases?
- [x] on what types can we use bitwise operators?
- [ ] 





## Topic for the next meetup

- 3 private field operator
- 1 lexical scope
- 1 Temporal.*
- 1 how to read the spec (as a beginner)
-   using

 



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
