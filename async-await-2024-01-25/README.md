# Async+Await - #jslang 68th Edition

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are async+await
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] What happens to errors inside async functions?
- [ ] Multiple awaits inside an async functions, and how they may interfere with each other?
- [x] Can we use an async function for building a "delay"?
- [ ] Can it block the code execution?
- [ ] Chaining awaits
- [ ] How far does async spreads?




## Topic for the next meetup

6  generators
2  nullish coalesing
2  `+` operator
1  pass by value/reference






## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
