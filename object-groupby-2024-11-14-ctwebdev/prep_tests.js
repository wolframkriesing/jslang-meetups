import assert from 'assert';

describe('how Object.groupBy works', () => {
  it('a noop function, sorts all values into an object`s prop `undefined`', () => {
    const items = [1, 'two', {three: 3}];
    const noop = () => undefined;
    assert.deepEqual(Object.groupBy(items, noop), {undefined: items});
  });
  it('given a list of numbers and a function that returns "odd"/"even", than it groups all numbers accordingly', () => {
    const numbers = [1, 2, 3, 4, 5, 6, 124, 9873];
    const evenOrOdd = x => x % 2 === 0 ? 'even' : 'odd';
    const expected = {odd: [1, 3, 5, 9873], even: [2, 4, 6, 124]};
    assert.deepEqual(Object.groupBy(numbers, evenOrOdd), expected);
  });
  it('given a string, and a function checking for upper/lower case, it groups them accordingly', () => {
    const aString = 'toUpperCase';
    const upperOrLower = char => char === char.toUpperCase() ? 'upper' : 'lower';
    const expected = {upper: ['U', 'C'], lower: ['t', 'o', 'p', 'p', 'e', 'r', 'a', 's', 'e']};
    assert.deepEqual(Object.groupBy(aString, upperOrLower), expected);
  });
});

describe('key generation of Object.groupBy()', () => {
  it('a key is the string representation of a property', () => {
    const arr = [1, 'x', 42];
    const actual = Object.groupBy(arr, x => x);
    const expected = {1: [1], x: ['x'], 42: [42]};
  });

  it('the first param must be iterable', () => {
    const maybeIterable = {};
    assert.equal(Symbol.iterator in maybeIterable, false);
  });
  it('passing an object as first param will throw an exception, because its not an iterable', () => {
    const fn = () => { Object.groupBy({}, x => x); };
    assert.throws(fn);
  });

  it('we can also groupBy over a string', () => {
    const grouped = Object.groupBy('hello', char => char);
    assert.deepEqual(grouped, {h: ['h'], e: ['e'], l: ['l', 'l'], o: ['o']});
  });
  it('so, a string iterable, right?', () => {
    const stringIterator = typeof ''[Symbol.iterator];
    assert.equal(stringIterator, 'function');
  });
});

const noop = () => {};

describe('spec: `2. Let obj be OrdinaryObjectCreate(null).`', () => {
  it('the object returned by Object.groupBy() has prototype null', () => {
    const grouped = Object.groupBy([], noop);
    assert.equal(grouped.prototype, null);
  });
  it('an object literal `{}` has the prototype of `Object`', () => {
    const newObject = {};
    assert.equal(Reflect.getPrototypeOf(newObject), Object.prototype);
  });
  it('a function has the prototypes of Function and Object in its chain', () => {
    const fn = () => {};
    assert.equal(Reflect.getPrototypeOf(fn), Function.prototype);
    assert.equal(Reflect.getPrototypeOf(Reflect.getPrototypeOf(fn)), Object.prototype);
    assert.equal(fn.__proto__.__proto__, Object.prototype); // same as above, just using the "inofficial" __proto__ property
    assert.equal(fn.__proto__.__proto__.__proto__, null); // the end of the prototype chain
  });
});

describe('spec text: `1. Perform ? RequireObjectCoercible(items).`', () => {
  it('means null throws', () => {
    const fn = () => { Object.groupBy(null); };
    assert.throws(fn);
  });
  it('means undefined throws', () => {
    const fn = () => { Object.groupBy(undefined); };
    assert.throws(fn);
  });
  it('means an array does not throw (at least in step 1 of the algorithm)', () => {
    const fn = () => { Object.groupBy([], noop); };
    assert.doesNotThrow(fn);
  });
});

describe('spec text: `4. Let iteratorRecord be ? GetIterator(items, sync).`', () => {
  it('the ? means it might throw, let it throw', () => {
    // null or undefined would throw because of step 1, so use an object
    assert.throws(() => { Object.groupBy({}, noop); });
  });
  it('a self made iterator with one value', () => {
    const iterable = {
      [Symbol.iterator]: () => ({next: () => ({value: 0, done: true})})
    };
    assert.deepEqual(Object.groupBy(iterable, noop), {});
  });
  it('a self made iterator with more values', () => {
    const iteratorStepValues = [
      {value: 23, done: false},
      {value: 42, done: false},
      {value: 0, done: true},
    ];
    const iterable = {
      [Symbol.iterator]: () => {
        return {next: () => iteratorStepValues.shift()};}
    };
    assert.deepEqual(Object.groupBy(iterable, noop), {undefined: [23, 42]});
  });
});

describe('spec: `i. Set key to Completion(ToPropertyKey(key)).`', () => {
  it('providing a [Symbol.toPrimitive] for a key', () => {
    const fn = () => ({[Symbol.toPrimitive]: () => 'my-key'});
    assert.deepEqual(Object.groupBy([1,2,3], fn), {'my-key': [1,2,3]});
  });
  it('return an object as key, without [Symbol.toPrimitive], the key will be `[object Object]`', () => {
    const fn = () => ({});
    assert.deepEqual(Object.groupBy([1,2,3], fn), {'[object Object]': [1,2,3]});
  });
  it('lets use the type hint passed to [Symbol.toPrimitive] as a key', () => {
    const fn = () => ({[Symbol.toPrimitive]: (typeHint) => typeHint});
    assert.deepEqual(Object.groupBy([1,2,3], fn), {'string': [1,2,3]});
  });
  it('the [Symbol.toPrimitive] is the new `toString` (since ES6)', () => {
    const obj = {};
    obj[Symbol.toPrimitive] = () => 'from toPrimitive';
    assert.equal(obj.toString(), '[object Object]');
    // BUT
    assert.equal(String(obj), 'from toPrimitive');
    assert.equal('' + obj, 'from toPrimitive');
  });

  it('a key can also be a Symbol', () => {
    const symbol = Symbol('my');
    const fn = () => symbol;
    const grouped = Object.groupBy([1,2], fn);
    assert.deepEqual(grouped[symbol],  [1,2]);
  });
});
