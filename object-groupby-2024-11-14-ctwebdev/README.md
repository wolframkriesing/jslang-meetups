# Object.groupBy - at c't webdev conference 2024-11-14

https://ctwebdev.de/

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [ ] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [ ] Introduce each other
3) [ ] Quickly align on what is **Object.groupBy**?
4) [ ] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn by writing a test

- [ ] 



## Topic for the next meetup



