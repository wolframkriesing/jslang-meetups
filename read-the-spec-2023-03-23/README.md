# How to read spec

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what is the specification
4) [ ] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] What is the spec?
- [x] Where to find the spec?
- [ ] What changed when?
- [ ] Are there significant/relevant version differences?
- [ ] Is there a most commonly used section?
- [ ] 

## Next Topics

1   array-like objects  
1   SharedArrayBuffer  
3   TypedArray  
6   prototype  
    Reflect  
