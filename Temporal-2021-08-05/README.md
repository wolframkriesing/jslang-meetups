# Temporal API

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what Temporal API is
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

The questions we would like to answer today:

1) [x] Calculate with dates (e.g. today+1day)
2) [x] Different calendars (japanese, Kerala, Gregorian, Muslim, ...)
3) [ ] Offset? (summer time, timezone, winter time, ...)
4) [ ] Conversion (from/to timezone)
5) [ ] Formatting
6) [x] How exact is the time this API can handle? (smallest unit) - nano seconds, using BigInt
7) [ ] 
8) [ ] 
9) [ ] 


## Topic for the next meetup

- 1 Temporal API   
- 1 Iterators
- 6 .at()
-   Atomics/SharedArrayBuffer
-   BigInt
- 
- 



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language

## References

- MDN page https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap
- ECMAScript Spec as used in the meetup
  - https://tc39.es/ecma262/#sec-weakmap-constructor

- This source code and other meetup's source https://gitlab.com/wolframkriesing/jslang-meetups