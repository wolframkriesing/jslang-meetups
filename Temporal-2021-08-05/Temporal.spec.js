import {strict as assert} from 'assert';
import {Temporal} from "@js-temporal/polyfill";

describe('Temporal API', () => {
  it('we have the `Temporal` object', () => {
    assert(Temporal);
  });
});

describe('Calculate with dates', () => {
  it('what index of the day was yesterday (the day we wrote the test)', () => {
    const today = Temporal.ZonedDateTime.from('2021-08-05T19:00[America/Los_Angeles]');
    const yesterday = today.subtract({days: 1});
    assert.equal(yesterday.day, 4);
  });
  it('what index of the day was yesterday', () => {
    const today = Temporal.Now.zonedDateTimeISO();
    const yesterday = today.day - 1;
    assert.equal(today.subtract({days: 1}).day, yesterday);
  });
  it('Temporal.Instant(0) returns 1.1.1970', () => {
    let instant = new Temporal.Instant(BigInt(0));
    assert.equal(instant.toString(), "1970-01-01T00:00:00Z");
  });
  it('calling "now" twice is a different time', () => {
    // WARNING: this test might be flaky and FAIL e.g. on a (really) fast machine.
    const time1 = Temporal.Now.instant();
    const time2 = Temporal.Now.instant();
    assert.notEqual(time1.toString(), time2.toString());
  });
  xit('call "now" twice (hopefully) at the same time, using promises', async () => {
    // WARNING: this test might be flaky and FAIL e.g. on a (really) fast machine.
    const result = await Promise.all([
      Promise.resolve(Temporal.Now.instant()),
      Promise.resolve(Temporal.Now.instant()),
    ]);
    assert.notEqual(result[0].toString(), result[1].toString());
  });
  xit('call "now" twice even more async', async () => {
    // WARNING: this test might be flaky and FAIL e.g. on a (really) fast machine.
    const result = await Promise.all([
      (async () => Temporal.Now.instant())(),
      (async () => Temporal.Now.instant())()
    ]);
    assert.notEqual(result[0].toString(), result[1].toString());
  });
});

describe('Calendars', () => {
  it('converting 5.Aug 2021 to indian calendar is "5/14/1943 Saka"', () => {
    const today = Temporal.PlainDate.from({ year: 2021, month: 8, day: 5 });
    const todayIndian = today.toLocaleString('en-US', { calendar: 'indian' });
    assert.equal(todayIndian, '5/14/1943 Saka');
  });
  it('converting 5.Aug 2021 to indian calendar is "शक 14/5/1943"', () => {
    const today = Temporal.PlainDate.from({ year: 2021, month: 8, day: 5 });
    const todayIndian = today.toLocaleString('hi-IN', { calendar: 'indian' });
    assert.equal(todayIndian, '14/5/1943 शक');
  });
  it('converting 31.Dec 2021 to indian calendar is "शक ???"', () => {
    const today = Temporal.PlainDate.from({ year: 2021, month: 12, day: 31 });
    const todayIndian = today.toLocaleString('en-US', { calendar: 'indian' });
    assert.equal(todayIndian, '10/10/1943 Saka');
  });
  it('convert from indian date to gregorian - throws (but we dont know yet why)', () => {
    const endOfCorona = Temporal.PlainDate.from({ year: 1945, month: 5, day: 9, calendar: 'indian' });
    assert.throws(() => {
      const gregorian = endOfCorona.toLocaleString('en-US', { calendar: 'gregory' });
    });
  });
  // it('create a date object from an indian date string', () => {
  //   const endOfCorona = Temporal.PlainDate.from('1945-05-09T00:00:00Z[Europe/Berlin][u-ca=indian]');
  //   assert.equal(endOfCorona.toString(), 'end of corona!!!');
  // });
});
