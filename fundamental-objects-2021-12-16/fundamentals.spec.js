import {strict as assert} from 'assert';

describe('`new Object` vs `Object()`', () => {
  const newObject = new Object();
  const objectFunction = Object();
  it('both are of type "object"', () => {
    assert.equal(typeof newObject, 'object');
    assert.equal(typeof objectFunction, 'object');
  });
  it('both have the same properties', () => {
    assert.deepEqual(Object.keys(newObject), Object.keys(objectFunction))
  });
  it('both have all the same own properties', () => {
    assert.deepEqual(Object.getOwnPropertyDescriptors(newObject), Object.getOwnPropertyDescriptors(objectFunction));
  });
});

describe('Function object', () => {
  it('the "name" property for an anonymous arrow function is set to the variable`s name it is assigned to', () => {
    const anonymousFunction = () => {};
    assert.equal(anonymousFunction.name, 'anonymousFunction');
  });
  it('the "name" property of an anonymous classic function is set to the variable`s name it is assigned to', () => {
    const classicFunction = function () {};
    assert.equal(classicFunction.name, 'classicFunction');
  });
  it('the "name" property of an unassinged anonymous arrow function is empty', () => {
    assert.equal((() => {}).name, '');
  });
  it('the "name" property of an unassigned anonymous `new Function()` is "anonymous"', () => {
    assert.equal((new Function()).name, 'anonymous');
  });
  it('the "name" property of an unassigned anonymous `new Function()` with a function body is "anonymous"', () => {
    assert.equal((new Function('return 0;')).name, 'anonymous');
  });

  it('the "name" property of the function when assinged to an object property has this property`s name', () => {
    const obj = {
      functionInObject: () => {}
    }
    assert.equal(obj.functionInObject.name, 'functionInObject');
  });
  it('reassigning a function to a different variable does NOT change its "name" property', () => {
    const obj = {
      function1: () => {}
    };
    const function2 = obj.function1;
    delete obj.function1;
    assert.equal(function2.name, 'function1');
  });

  it('the "name" property is taken from the function name, not from the variable, when both are given', () => {
    const varName = function funcName() {};
    assert.equal(varName.name, 'funcName');
  });
  it('the "name" property can be modified', () => {
    const someFn = () => {};
    assert.throws(() => {
      someFn.name = 'somethingElse';
    }, TypeError);
  });
  it('the "name" property is writeable=false', () => {
    const fn = () => {};
    assert.equal(Object.getOwnPropertyDescriptor(fn, 'name').writable, false);
  });

  it('an arrow functions own properties are "name" and "length"', () => {
    const fn = () => {};
    const expected = {
      name: {
        value: 'fn',
        writable: false,
        enumerable: false,
        configurable: true,
      },
      length: {
        value: 0,
        writable: false,
        enumerable: false,
        configurable: true,
      }
    };
    assert.deepEqual(Object.getOwnPropertyDescriptors(fn), expected);
  });
});

describe('Boolean constructor', () => {
  it('has NO property "value"', () => {
    const bool = Boolean();
    assert.equal(bool.hasOwnProperty('value'), false);
  });
  it('has NO own properties', () => {
    assert.deepEqual(Object.getOwnPropertyDescriptors(Boolean()), {});
  });
});

describe('`Error`', () => {
  it('the simplest `new Error()` only has own property "stack"', () => {
    const propertyNames = Object.getOwnPropertyNames(new Error());
    assert.deepEqual(propertyNames, ['stack']);
  });
  it('`new Error("")` passing a message adds the property "message"', () => {
    const propertyNames = Object.getOwnPropertyNames(new Error('something'));
    assert.deepEqual(propertyNames, ['stack', 'message']);
  });
});
