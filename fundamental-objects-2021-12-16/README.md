# Fundamental Objects

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what Fundamental Objects are
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [ ] What are Fundamental Objects?
- [ ] How can an Object become a Symbol?
- [x] `new Object` vs `Object()` (vs `{}`)
- [x] `new Function`, `Function`, `() => {}`, `function() {}`
- [x] properties+function/method on each Fundamental Object
- [ ] `Error` and `throw ???`


## Topic for the next meetup

- temporal
- the `Error`
- DataView



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
