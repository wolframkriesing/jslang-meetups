# Async Iterators - for await of

## The plan

The plan for the meetup was to:

1. Collect items that we want to learn about type conversion
1. Write tests to learn, verify and describe some type conversion properties. 

## How we ran the meetup

We organized the meetup on [meetup.com][meetup].

## Onsite

We started by introducing each other shortly.
Then we did 1. and collected items. We came up with this:
* [x] for-of with async+await
* [x] having await inside forEach
* [ ] iterate over list of promises
  - reject one promise (of the array)
  - for-await for regular array
  - await inside of the loop
  - promise order depends on timing?
* [ ] iterate over generator which yields promises
* [ ] write our own forEachAsync()



## Recap


## Links

MDN page for for-await  
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for-await...of

Bluebird, a promise lib we talked about briefly  
http://bluebirdjs.com/docs/getting-started.html

Promise frites a tiny library which makes working with promises easy  
https://github.com/webpapaya/promise-frites/blob/master/doc.md

What the heck is the event loop anyway? | Philip Roberts | JSConf EU  
https://www.youtube.com/watch?v=8aGhZQkoFbQ

