# DataView

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what DataView
4) [ ] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] buffer is (im)mutable?
- [ ] write 4 int16's and read 1 64 bit int
- [ ] too small arraybuffer (e.g. 1 byte, write 4 bytes)
- [ ] does at(-1) work?
- [x] set one endianness and read in another
- [ ] set as uint and read as int (is the 1st bit the sign?)
- [ ] write a custom DataView?
- [ ] multiple dataviews on one buffer
- [ ] de/encoding using ArrayBuffer/DataView? base64, etc.

## Topic for the next meetup

- BigInt
- WeakMap
- [].flat(), [].flatMap()
- toString()



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
