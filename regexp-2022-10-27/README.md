# RegExp (Regular Expression) Objects (ECMAScript Spec Chapter 22.2)

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are regexps
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] Ways to create regexps (objects, literal, ...), (how) do they differ?
- [ ] What methods are on a regexp?
- [ ] sequences e.g. /d 
- [ ] Which methods accept regexps?
- [x] matched results, groups - how to use them?



## Topic for the next meetup

3   what's the latest ecma features?
7   async generators
7   proxies
    Symbol
    [].at()



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
