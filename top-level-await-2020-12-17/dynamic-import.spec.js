import {strict as assert} from 'assert';

const fastestFile = await Promise.any([
  import('./slow.js'),
  import('./not-existing.js'),
  import('./fast.js'),
]);

it('fastest import in a top level await wins', () => {
  assert.equal(fastestFile.default, 'fast-file');
});

xit('a stringified dynamic import starts with [Module]', async () => {
  const fastFile = await import('./fast.js');
  assert.equal(fastFile, '[Module] ...');
});
