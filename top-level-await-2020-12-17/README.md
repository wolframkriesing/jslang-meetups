# Top Level Await

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what top level await is 
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

The questions we would like to answer today:

1) [x] How run the code in a test?
2) [x] What's the problem that it solves? - No need for IIFE
3) [x] What are the consequences when an error is thrown?
4) [x] Importing another module, when is that code executed? What about failures. 
5) [x] What is the advantage to ...? - Readability




## Topic for the next meetup

-   Reflect
- 6 Modules
- 1 trimStart/trimEnd
- 1 RegExp
- 5 WeakMap/WeakSet



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language

## References

- the proposal https://tc39.es/proposal-top-level-await/
- a good article on top level await https://dandkim.com/top-level-await/

- The tc39 page https://tc39.es/
- Practice JavaScript online
- This source code and other meetup's source https://gitlab.com/wolframkriesing/jslang-meetups