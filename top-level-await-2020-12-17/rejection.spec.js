import {strict as assert} from 'assert';

try {
  await Promise.reject();
  it('a top level that throws can be caught, we should never see this', () => {
    assert(false);
  });
} catch {
  it('a top level that throws can be caught', () => {
    assert(true);
  });
}
