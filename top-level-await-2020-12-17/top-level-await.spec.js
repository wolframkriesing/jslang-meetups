import assert from 'assert';

const x = await 1;

describe('Top level await - How to test it?', () => {
  it('an await on the top level (see above) had been working', () => {
    assert.equal(x, 1);
  });
  it('an await inside eval throws, because ... (maybe we find out on twitter, #jslang)', () => {
    assert.throws(() => { eval('await 1') }, SyntaxError);
  });
});

const before = new Date().getTime();
const after = await new Promise((resolve) => {
  setTimeout(() => resolve(new Date().getTime()), 3000);
});
it('top level await blocks the execution until it resolves', () => {
  assert(before + 2000 < after);
});
