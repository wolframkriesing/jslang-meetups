import {strict as assert} from 'assert';

describe('is the catch statement required?', () => {
  it('a lone try-statement throws a SyntaxError', () => {
    assert.throws(() => eval('try {}'), SyntaxError);
  });
  it('a try statement with a finally, without a catch is valid syntax', () => {
    assert.doesNotThrow(() => eval('try{}finally{}'));
  });
});

describe('how does eval work? (a little bit)', () => {
  it('eval(valid statement) returns undefined', () => {
    assert.equal(eval('try{}finally{}'), undefined);
  });
  it('eval(valid statement) can return something', () => {
    assert.equal(eval('try{1}finally{}'), 1);
  });
});

describe('can we nest try-catches?', () => {
  it('try catch can be nested', () => {
    let innerCatchWasCalled = 'none';
    try {
      try {
        throw Error('inner');
      }
      catch(e) {
        innerCatchWasCalled = e.message;
      }
    } catch {
      throw Error('should NOT get here!!!');
    }
    assert.equal(innerCatchWasCalled, 'inner')
  });
});