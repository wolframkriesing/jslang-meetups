# try-catch

Let's figure out what we can learn about it.

## Agenda

Onsite: 🍏 🍕 🥤 by **Mister Spex** https://www.misterspex.de/  
Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are try+catch
4) [ ] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] is the catch statement required?
- [ ] can I trigger a catch without an exception? 
- [x] is e in `catch(e)` required? (learned this implicitly)
- [ ] is there something that can not be thrown?
- [ ] can you throw a (generator) function, etc.?
- [x] can we nest try-catches?



## Topic for the next meetup

2  eval
__5  try-finally (without catch)__
4  labels
1  Reflect
   new Function(), Function()
1  read the spec
5  break







## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
