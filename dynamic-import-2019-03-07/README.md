# dynamic import()

## The plan

The plan for the meetup was to:

1. Collect items that we want to learn about dynamic import()
1. Write tests to learn, verify and describe some type conversion properties. 

## How we ran the meetup

We organized the meetup on [meetup.com][meetup].

## Onsite

We started by introducing each other shortly.
Then we did 1. and collected items. We came up with this:
* why
* cyclic imports
* dynamic strings as module names
* .... and more

## Recap

We started with a simple test of what we expect would fail. Since we had
Daniel Ehrenberg from TC39 with us, we switched plans and let him walk us
through the spec for dynamic imports.

A few notes for the modules specification:

- static imports make static analysis possible
- import is not a function, it's a keyword
- promiseCapability is a promise plus its resolve and reject functions
- modules always need to be utf-8 encoded!
- modules always check CORS
- modules always check JavaScript MIME-type
- never check for error strings, the specification only defines the type
  of errors
- if you want to improve EcmaScript, you might want to add error codes to 
  the specification

## Links

- [The Dynamic Imports Proposal repo](https://github.com/tc39/proposal-dynamic-import)
- [The Proposal's spec](https://tc39.github.io/proposal-dynamic-import/)
- touched on by Daniel [CSS WG Issue "Define which subresources block the DOM load event"](https://github.com/w3c/csswg-drafts/issues/1088)
- whatwg html PR [JSON Module Support](https://github.com/whatwg/html/pull/4407)
- ["modulepreload" in the HTML Spec](https://html.spec.whatwg.org/multipage/links.html#link-type-modulepreload)

[meetup]: https://www.meetup.com/JavaScript-The-Language/events/258970667/