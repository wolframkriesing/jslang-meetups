import assert from 'assert';

it('import throws when no parameter given', () => {
  assert.throws(() => { eval('import();') });
});
it('import throws when empty string is given', async () => {
  try {
    await import("");
    assert(false);
  } catch(e) {
  }
});
it('import throws when empty string is given', async () => {
  try {
    await import("");
    assert(false);
  } catch(e) {
    // assert.equal("TypeError: Failed to resolve module specifier ''", e.toString()); runs only in Chrome, and we dont know when the string changes, since it is not specified
      
    // assert(e instanceof TypeError); // in the browser we get this
    assert(e instanceof Error); // on node we get this 
  }
});

// use a variable

xit('dynamic import should not throw for an existing valid JS file', () => {
  import('./file.js');
});