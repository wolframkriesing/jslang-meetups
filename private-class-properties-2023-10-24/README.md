# Private Class Properties

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what is the Private Class Properties?
4) [x] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [ ] Are private properties really private?
- [ ] A constructor can be not private?
- [ ] Can we make computed properties private? 
- [ ] How to use private getters, setter, statics, methods, fields
- [ ] 





## Topic for the next meetup


 



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
