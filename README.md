# JavaScript the Language Meetups

This is a meetup, which was started in Munich, where we are only handling JavaScript
features. No libraries, no frameworks, just language features such as Promises, Generators, 
functions, destructuring and so on.

In this repository we will collect whatever we see as relevant for sharing or collecting.
We might share some of our code, links, etc.
If all that looks useless to you feel free to move on. If it inspires you to do the 
same or just reuse ideas you are welcome to do so.

## Run the repo

- `docker-compose run node bash` opens a node shell (you just need docker, no nodejs globally installed ;) )
  - `npm test` run all tests
  - `mocha ./prototype-2021-11-18/prototype.spec.js` run the tests of the November 2021 meetup on prototypes 

## Social Media

Find us on [twitter under the hashtag #jslang][#jslang].

## Past meetups

- 2015, Oct 27th - Generators
- 2015, Nov 24th - [Proxies](./proxies-2017-11-09)
- 2016, Jan 26th - Destructuring
- 2016, Mar 22nd - [Promises](http://kata-log.rocks/clock-in-kata)
- 2016, Apr 26th - Promises
- 2017, Jun 1st - async, await
- 2017, Jul 6th - classes, protoype
- 2017, Aug 10th - [Generators](./generators-2017-08-10)
- 2017, Sep 7th - [Functions](./functions-2017-09-07)
- 2017, Oct 5th - [SharedArrayBuffer](./SharedArrayBuffer-2017-10-05)
- 2017, Nov 9th - [Proxies](./proxies-2017-11-09)
- 2018, Feb 5th - [Template literals](./template-literals-2018-02-15)
- 2018, Mar 22nd - [Function](./functions-2018-03-23)
- 2018, Apr 19th - Map, Set, WeakMap
- 2018, Jun 7th - Generators
- 2018, Sep 18th - [Type coercion](./type-conversion-2018-09-18)
- 2018, Oct 18th - [Errors](./errors-2018-10-18)
- 2018, Nov 22nd - [Proxies](./proxies-2018-11-22)
- 2018, Dec 12th - [Labels](./labels-2018-12-12)
- 2019, Jan 24th - [The Global Object](./global-object-2019-01-24)
- 2019, Feb 14th - [Async Iterations (for-await-of)](./async-iterators-2019-02-14)
- 2019, Mar 7th - [import() as a function](./dynamic-import-2019-03-07)

## Future Meetup Ideas

### Generators without ES6

We have implemented the [file transmitter using generators][file-transmitter]. In order to see how useful 
generators are we should implement the function without generators.

[#jslang]: https://twitter.com/search?q=%23jslang
[file-transmitter]: ./generators-2017-08-10/file-transmitter.spec.js

## How to run this

```
$ # Start a container running node
$ docker-compose up --detach

$ # Enter the container running node
$ docker exec -it node bash
root@fa788a:/app# node -v
v15.0.1

$ # OR Run the tests without "entering" the container
$ docker exec -it node npm test

> jslang-meetups@1.0.0 test
> mocha -r esm **/*.spec.js --bail

....

```
