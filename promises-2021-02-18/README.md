# Promises

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
       Repo: https://codeberg.org/wolframkriesing/jslang-meetups
2) [x] Introduce each other
3) [x] Quickly align on what are "Promises" 
4) [x] Collect what we want to learn 
5) [x] Test drive the code, together
6) [x] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

The questions we would like to answer today:

1) [x] How to collect more than one promise (in a loop) 
2) [ ] What functions exist on `Promise`?
3) [ ] Immediatly resolved promises, Order of execution
4) [ ] Compare promise/async+await behaviors
5) [ ] Promise.allSettled
6) [x] Is there `finally`?
7) [ ] Learn more about Promise.race(), and its edge cases
8) [ ] map/flatMap and Promise.then resolving with a promise instead of a value
9) [ ] how to cancel a promise (or multiple promises)
10)[ ] Promise constructor (passing a Promise to it) - Promise stacking



## Topic for the next meetup

-  1 new Error() vs Error(), Promise() vs new Promise()
-  2 for-await
- 11 continue on Promises
-  2 `continue` `break` `with`
-    ===
-  6 unary operators, + - ~ ! 
-    ?.

-   Map, Set
- 4 WeakMap
- 2 Iterator object
- 3 bind, call, apply
- 2 try catch

## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language

## References
- https://tc39.es/ecma262/#sec-promise.any

- The tc39 page https://tc39.es/
- Practice JavaScript online
- This source code and other meetup's source https://gitlab.com/wolframkriesing/jslang-meetups