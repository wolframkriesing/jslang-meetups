# Class (and Prototype)

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what is the Class (and Prototype)
4) [x] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] static fields
- [x] anonymous class
- [ ] thing.prototype vs Object.getPrototypeOf(thing)
- [ ] prototypal inheritance 
- [ ] reuse functions across mulitple classes
- [x] inheritance
- [ ] 





## Topic for the next meetup

6  static initialization blocks `static { ... }`
   function.bind()
1  import
1  Map (new features)
2  type coercion, +{}, -[], 1+new Date(), {}+[], ...   

 



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
