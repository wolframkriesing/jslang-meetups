# BigInt

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what BigInt
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [ ] what's the limit?
- [x] Number.MAX_VALUE + 1
- [ ] BigInt and regular operators?
- [x] BigInt+Number?
- [ ] Compare BigInt and Number
- [ ] BigInt divided BigInt ???
- [x] Serialize a BigInt
- [x] Literal???
- [x] JSON-ifying???
- [ ] parseInt

## Topic for the next meetup

- toJSON()
- flatMap(), flat(), ...
- top level await



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
