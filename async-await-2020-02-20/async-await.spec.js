import assert from 'assert';

describe('async/await', () => {
  it('a sync function can be awaitable', async () => {
    const cb = () => 1;
    const result = await cb();
    assert.equal(result, 1);
  });
  it('a value can be awaitable', async () => {
    const result = await 1;
    assert.equal(result, 1);
  });
  it('undefined can be awaitable', async () => {
    const result = await undefined;
    assert.equal(result, undefined);
  });
  it('await throw is not possible', () => {
    assert.throws(() => eval(`(async () => {
      await throw 1;
    })()`), SyntaxError);
  });
  it('blocks can not be awaited', () => {
    assert.throws(() => eval(`(async () => {
      await {
        const x = 1;
      }
    })()`), SyntaxError);
  });
  it('await without async throws SyntaxError', () => {
      assert.throws(
        () => eval(`await 1`),
        SyntaxError
      );
  });
  it('type of async function is an function', () => {
    const asyncFunction = async () => {};
    assert.equal(typeof asyncFunction, "function");
  });
  it('instance of async function is an Object', () => {
    const asyncFunction = async () => {};
    assert.equal(asyncFunction instanceof Object, true);
  });
  it('return type of async function is Promise', () => {
    const asyncFunction = async () => 1;
    return asyncFunction().then(result => assert.equal(result, 1));
  });
  it('await a Promise returns resolved value', async () => {
      const promise = Promise.resolve(1);
      const result = await promise;
      assert.equal(result, 1);
  });
  it('await a nested Promise returns resolved value', async () => {
      const promise = Promise.resolve(Promise.resolve(1));
      const result = await promise;
      assert.equal(result, 1);
  });
  it('await rejected promise throws', async () => {
      const promise = Promise.reject(1);
      try {
        const result = await promise;
      } catch(error) {
        return assert.equal(error, 1);
      }
      assert.fail('Expected to catch');
  });
  xit('await do not yield generator function', async() => {
    function *g() {
      let x= 0;
      while(true) {
        x += 1;
        yield x;
      }
    }
    const result = await g();
    assert.deepEqual(result, {});
  });
});