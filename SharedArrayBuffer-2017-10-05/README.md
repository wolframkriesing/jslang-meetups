# SharedArrayBuffer

A topic that we had all been excited about, but noone had knowledge about.
So we dived into the unknown. A fun experience.

## The Plan

The plan was to:

1) learn how web workers work
2) use SharedArrayBuffer with it

## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/de-DE/preview/JavaScript-The-Language/events/243182533

We introduced each other and then started right away writing some tests for 
learning how WebWorkers work, which we need for using SharedArrayBuffer.
That was a good learning already by itself.
Later we used the SharedArrayBuffer and wrote tests on how they use this shared buffer,
we experimented a little bit to figure out how the timing behaves but didn't get to
real conclusions there.

## References

- MDN page on [SharedArrayBuffer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SharedArrayBuffer)
- MDN page on [WebWorker](https://developer.mozilla.org/en-US/docs/Web/API/Worker)
- HMTL spec on [`postMessage`](https://html.spec.whatwg.org/multipage/workers.html#dom-worker-postmessage)
- little brother [ArrayBuffer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer)
- MDN page on [TypedArray](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray)
- "ECMAScript Shared Memory and Atomics" in the [ECMAScript spec](https://tc39.github.io/ecmascript_sharedmem/shmem.html)
- Article by Axel Rauschmayer on ["Shared memory and atomics"](http://2ality.com/2017/01/shared-array-buffer.html)
