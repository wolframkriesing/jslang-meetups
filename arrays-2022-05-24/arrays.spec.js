import {strict as assert} from 'assert';

describe('How can an array get bigger?', () => {
  it('push()`ing a new element into an empty array makes it of length 1', () => {
    const array = [];
    array.push('i dont know');
    assert.equal(array.length, 1);
  });
  it('setting a value on index 0 will result in length 1', () => {
    const array = [];
    array[0] = 'I know';
    assert.equal(array.length, 1);
  });
  it('inserting an element at index 5 results in an array of length 6', () => {
    const array = [];
    array[5] = 'last';
    assert.equal(array.length, 6);
  });
  it('fill() an array with 3 elements, will only "change" existing elements', () => {
    const array = [0, 0, 0];
    assert.deepEqual(array.fill(1, 2, 1000), [0, 0, 1]);
  });
  it('fill()ing an array with start=3 end=1 it will NOT throw', () => {
    const array = [0, 0, 0];
    assert.doesNotThrow(() => array.fill(1, 3, 1));
  });
  it('fill()ing an array with start=3 and end=1 will flip start/end', () => {
    const array = [0, 0, 0];
    assert.deepEqual(array.fill(1, 3, 1), [0, 0, 0]);
    // Because of "6. Else, let k be min(relativeStart, len)."
    // in https://tc39.es/ecma262/#sec-array.prototype.fill
  });
  it('a negative {-end} will end at length+{-end}', () => {
    const array = [0, 0, 0];
    assert.deepEqual(array.fill(5, 0, -2), [5, 0, 0])
  });
  it('fill("x") an array of three elements, will result in an array of "x"s', () => {
    const array = [0, 0, 0];
    assert.deepEqual(array.fill(7), [7, 7, 7]);
  });
  it('creating an array via Array(5) and fill(x) will result in an array full of x', () => {
    const array = Array(5);
    assert.deepEqual(array.fill(7), [7,7,7,7,7]);
  });
});

describe('poke holes into an array', () => {
  it('prefilled array of length x AND poke a hole at pos 2+3 THEN the length remains the same', () => {
    const array = [0, 1, 2, 3, 4];
    delete array[2];
    delete array[3];
    assert.equal(array.length, 5);
  });
  it('poking a hole into an array via `delete` and setting to `undefined` is NOT the same', () => {
    const array = [0, 1, 2, 3, 4];
    delete array[2];
    array[3] = undefined;
    assert.deepEqual(array, [0, 1, , undefined, 4])
  });
  it('poke a hole into a one-element array via `delete`, then [0]===undefined', () => {
    const array = [42];
    delete array[0];
    assert.equal(array[0], undefined);
    // BUT NOT
    assert.notDeepEqual(array, [undefined]);
  });
  it('`delete`ing the only element of an array, makes it length=1', () => {
    const array = [1];
    delete array[0];
    assert.equal(array.length, 1);
  });
  it('overriding an array with an empty array, makes it length=0', () => {
    let array = [1];
    array = [];
    assert.equal(array.length, 0);
  });
});

describe('accessing array elements', () => {
  it('an array is of type object', () => {
    assert.equal(typeof [], 'object');
  });
  it('adding a property "foo" to an array is like an object-property', () => {
    const array = [];
    const myObject = {x: 'z'};
    array.foo = myObject;
    assert.equal(array.foo, myObject);
  });
  xit('the array is an "enumerable"', () => {
    // NOT sure how to find out if an array is an enumerable. Maybe its not ;(
    const array = [0];
    const descriptor = Object.getOwnPropertyDescriptor(array);
    assert.equal(descriptor.enumerable, true);
  });
});
