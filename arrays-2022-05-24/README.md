# Arrays

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are arrays
4) [x] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] can an array get smaller again?
- [x] what if you poke holes into an array?
- [x] index out of bounds
- [x] can an array get bigger? adding stuff after declaring it
- [ ] what array methods exist? e.g. at()
- [ ] how many levels deep does spread go?
- [ ] How to access elements? e.g. by string, by number, by variable names, by ???
- [ ] performance
- [ ] 



## Topic for the next meetup

- arrow functions
- how to read the spec
- functions
- types




## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
