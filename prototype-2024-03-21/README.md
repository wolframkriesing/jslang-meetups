# Prototype - #jslang 70th Edition

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what is prototype
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn by writing a test

- [x] creating a bare object give it prototype
- [ ] extending on object
- [ ] using parent class properties in the child class
- [x] diff between class and classical prototype
- [ ] is there a minimum prototype




## Topic for the next meetup





## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
