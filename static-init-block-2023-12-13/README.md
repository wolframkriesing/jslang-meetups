# Static initialization blocks

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are static initialization blocks
4) [x] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [ ] the blocks are not hoisted?
- [x] in what order are blocks executed?
- [x] order: constructor/blocks 
- [ ] null vs undefined





## Topic for the next meetup

6  async+await
4  new functions on `Map()`
   call, apply, bind 
3  FinalizationRegistry
 



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
