import assert from 'assert';

describe('String#trim*()', () => {
    it('trims a RTL text at the start (on the right)', () => {
        const rtlText = ' مرحبا بالعالم ';
        assert.equal(rtlText.trimStart(), 'مرحبا بالعالم ');
    });
    it('trimLeft and trimStart are the same function', () => {
        const rtlText = ' مرحبا بالعالم ';
        assert.equal(rtlText.trimStart, rtlText.trimLeft);
    });

    it('concatinating a RTL+LTR text ...', () => {
        const rtlText = ' مرحبا بالعالم ';
        const ltrText = ' Hello World ';
        assert.equal(rtlText + ltrText, ' مرحبا بالعالم ' + ' Hello World ');
    });
    it('trimEnd a RTL+LTR string', () => {
        const rtlText = ' مرحبا بالعالم ';
        const ltrText = ' Hello World ';
        assert.equal((rtlText + ltrText).trimEnd(), ' مرحبا بالعالم ' + ' Hello World');
    });
    it('trimEnd a RTL+LTR string', () => {
        const rtlText = ' مرحبا بالعالم ';
        const ltrText = ' Hello World ';
        assert.equal((ltrText + rtlText).trimEnd(), ' Hello World ' + ' مرحبا بالعالم');
    });
    it('splitting a RTL string, returns a "reversed" array', () => {
        const rtlText = ' حبا';
        assert.equal(rtlText.split('')[0], ' ');
    });

    it('call trim on an array, is not failing', () => {
        assert.doesNotThrow(() => String.prototype.trimStart.call([0, 1, 2]));
    });
    it('calling trimStart on an array returns the same array', () => {
        assert.equal(String.prototype.trimStart.call([0, 1, 2]), [0, 1, 2].toString());
    });
    it('calling trimStart on array with two first elements are whitespaces, it removes the first one', () => {
        assert.equal(String.prototype.trimStart.call([' ', ' ', 0, 1, 2]), ['', ' ', 0, 1, 2].toString());
    });

});
describe('Array sort', () => {
    it('calling sort on a sorted list of objects keep order the same', () => {
        const list = [{a: 1, b: 1}, {a: 1, b: 2}];
        assert.deepEqual(list.sort(() => 0), list);
    });
});
describe('Array flatMap', () => {
    it('a simple 2d array, mapped identity is flattened', () => {
        const arr = [[1],[2]];
        assert.deepEqual(arr.flatMap(a => a), [1,2]);
    });
    it('a 3d array, mapped identity is flattened one level deep', () => {
        const arr = [[[1]],[2]];
        assert.deepEqual(arr.flatMap(a => a), [[1],2]);
    });
    it('a 3d array, mapping `+1` is flattened one level deep', () => {
        const arr = [[[1]],[2]];
        assert.deepEqual(arr.flatMap(a => a+1), ['11','21']);
    });
    it('a 3d array, mapping `x[0]+1` is flattened one level deep', () => {
        const arr = [[[1]],[2]];
        assert.deepEqual(arr.flatMap(a => a[0]+1), ['11', 3]);
    });
    it('[[[1]]].toString()', () => {
        assert.equal([[[1]]].toString(), '1');
    });
    it('[[[1],2]].toString()', () => {
        assert.equal([[[1],2]].toString(), '1,2');
    });
    it('flatMap an object', () => {
        assert.deepEqual([{count: 42}].flatMap(obj => [obj.count]), [42]);
    });
    it('flatMap an object, without need to flatten', () => {
        assert.deepEqual([{count: 42}].flatMap(obj => obj.count), [42]);
    });
    it('flatMap an object, without 1 level flattening', () => {
        assert.deepEqual([{count: 42}].flatMap(obj => [[obj.count]]), [[42]]);
    });
});