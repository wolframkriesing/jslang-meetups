# operators

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what operators are
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [x] Choose next meetup topic
7) [x] Hangout ...

## Collect what we want to learn

The questions we would like to answer today:

1) [x] true+false - how operators work?
2) [ ] difference to `!x` vs. `foo === false`
3) [x] can unary operators create an infinite sequence?
4) [x] `void x` ???
5) [x] `-undefined`
6) [ ] `+new Date` vs `0+new Date`
7) [ ] how strict equal operator works with different data types?
8) [ ] `++x` vs `x++`
9) [ ] What is `~x`?


## Topic for the next meetup

- 1 import meta
- 7 temporal API
- 1 try catch throw finally
- 1 iterators



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language

## References

- MDN page https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap
- ECMAScript Spec as used in the meetup
  - https://tc39.es/ecma262/#sec-weakmap-constructor

- This source code and other meetup's source https://gitlab.com/wolframkriesing/jslang-meetups