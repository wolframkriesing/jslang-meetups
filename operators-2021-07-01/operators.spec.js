import {strict as assert} from 'assert';

describe('true+false - how operators work?', () => {
  it('`true+false` evaluates to `1`', () => {
    assert.equal(true+false, 1);
  });
  it('`true+true` evaluates to `2`', () => {
    assert.equal(true+true, 2);
  });
  it('`true+true+true` evaluates to `3`', () => {
    assert.equal(true+true+true, 3);
  });
  it('`false+true` evaluates to `1`', () => {
    assert.equal(false+true, 1);
  });
  it('`+true` evaluates to `1`', () => {
    assert.equal(+true, 1);
  });

  it('`+"true"` evaluates to `NaN`', () => {
    assert(Number.isNaN(+"true"));
  });
  it('`+"1true"` does not equal `Number.parseInt("1true")`', () => {
    assert(Number.isNaN(Number.parseInt("1true")) === false);
  });
  it('`Number("1true")` is NaN', () => {
    assert(Number.isNaN(Number("1true")));
  });

  it('`+variable` where `variable=true`, evaluates to 1', () => {
    const variable = true;
    assert.equal(+variable, 1);
  });
});

describe('can unary operators create an infinite sequence?', () => {
  it('+-+-!+~-0 does not throw', () => {
    assert.doesNotThrow(() => {
      +-+-!+~-0;
    });
  });
  it('~-0 evaluates to -1 (bitwise NOT of -0)', () => {
    assert.equal(~-0, -1);
  });
  it('~+0 evaluates to -1', () => {
    assert.equal(~+0, -1);
  });
  it('~0 evaluates to -1', () => {
    assert.equal(~0, -1);
  });
  it('`+- typeof +- void !+ delete ~-0` does not throw', () => {
    assert.doesNotThrow(() => {
      +- typeof +- void !+ delete ~-0;
    });
  });
});

describe('void ???', () => {
  it('void "coffee" is undefined', () => {
    assert.equal(void "coffee", undefined);
  });
  it('`void throw ???` throws', () => {
    assert.throws(() => {
      void (() => { throw (void 0);})()
    });
  });
});

describe('`-undefined` and `+new Date` vs `0+new Date`', () => {
  it('`-undefined` evaluates to `NaN`', () => {
    assert(Number.isNaN(-undefined));
  });
});
