# Prototype Object

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what is the Prototype Object
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [ ] Is it Prototype Object or Object Prototype?
- [x] create own object with a custom prototype
- [ ] messing around with builtin objects
- [ ] Does a class have a prototype?
- [ ] If you ask an object for a property, does it look in the prototype?
- [x] Do functions have a prototype?
- [ ] Is there a difference between a builtin function and a custom function?






## Topic for the next meetup

   classes
3  + operator
1  Proxy
7  Reflect
   Promises


 



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
