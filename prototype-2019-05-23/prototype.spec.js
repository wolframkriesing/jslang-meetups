import assert from 'assert';

describe('How it works???', () => {
    it('object literal has NO property "prototype"', () => {
        assert.strictEqual('prototype' in {}, false);
    });
    it('object literal has a prototype of Object', () => {
        assert.strictEqual(Reflect.getPrototypeOf({}), Object.prototype);
    });
    it('Object built-in has property prototype', () => {
        assert.strictEqual('prototype' in Object, true);
    });
    it('Object built-in has prototype of Function', () => {
        assert.strictEqual(Reflect.getPrototypeOf(Object), Function.prototype);
    });
    it('new Object() has prototype of Object', () => {
        assert.strictEqual(Reflect.getPrototypeOf(new Object()), Object.prototype);
    });
    it('Object.create() has prototype of Object', () => {
        assert.strictEqual(Reflect.getPrototypeOf(Object.create(null)), null);
    });
    it('Number built-in has a prototype', () => {
        assert.strictEqual('prototype' in Number, true);
    });
    it('new Number(7) built-in has NO prototype', () => {
        assert.strictEqual('prototype' in (new Number(7)), false);
    });
    it('new Number(7) === 7', () => {
        assert.strictEqual(typeof new Number(7), 'object');
        assert.strictEqual(typeof 7, 'number');
        // assert.strictEqual(new Number(7), 7);
    });
    // it('7 has NO prototype', () => {
    //     assert.strictEqual('prototype' in 7, false);
    // });
    it('Function built-in has a prototype', () => {
        assert.strictEqual('prototype' in Function, true);
    });
    it('Reflect.getPrototypeOf() for Number(7)', () => {
        assert.strictEqual(Reflect.getPrototypeOf(new Number(7)), Number.prototype);
    });
    it('typeof of Number.prototype?', () => {
        assert.strictEqual(typeof Number.prototype, 'object');
    });

    it('the prototype of "Object.prototype"', () => {
        assert.strictEqual(Reflect.getPrototypeOf(Object.prototype), null);
    });
    it('the prototype of "Function.prototype"', () => {
        assert.strictEqual(Reflect.getPrototypeOf(Function.prototype), Object.prototype);
    });
    it('the prototype of "Function.prototype.prototype"', () => {
        assert.strictEqual(
            Reflect.getPrototypeOf(
                Reflect.getPrototypeOf(Function.prototype)), null);
    });
});

describe('Learning about ...', () => {
    it('undefined as an object property, is possible', () => {
        const obj = {undefined: 4};
        assert.strictEqual(obj.undefined, 4);
    });
    it('prototype as an object property, is possible', () => {
        const obj = {prototype: 4};
        assert.strictEqual(obj.prototype, 4);
    });
    it('use "in" to test for "prototype"', () => {
        const obj = {prototype: 4};
        assert.strictEqual('prototype' in obj, true);
    });
    it('"in" does ???', () => {
        const obj = {something: undefined};
        assert.strictEqual('something' in obj, true);
        assert.strictEqual(obj.something, undefined);
        assert.strictEqual(obj.a, undefined);
    });
});

describe('What can we do with the prototype?', () => {
    it('Inheritance over 2 levels, find the prototypes', () => {
        class A {};
        class B extends A {};
        const b = new B();
        
        assert.strictEqual(Reflect.getPrototypeOf(B), A);
        assert.strictEqual(Reflect.getPrototypeOf(b), B.prototype);
        assert.strictEqual(
            Reflect.getPrototypeOf(Reflect.getPrototypeOf(b)), 
            A.prototype);
        assert.strictEqual(
            Reflect.getPrototypeOf(
                Reflect.getPrototypeOf(
                    Reflect.getPrototypeOf(b))), 
            Object.prototype);
    });
    it('Inheritance over 2 levels, old style', () => {
        const A = function() {};
        // const B = function() { return A(); };
        const B = function() {};
        Reflect.setPrototypeOf(B, A);
        const b = new B();
        
        assert.strictEqual(Reflect.getPrototypeOf(B), A);
        assert.strictEqual(Reflect.getPrototypeOf(b), B.prototype);
        // assert.strictEqual(
        //     Reflect.getPrototypeOf(Reflect.getPrototypeOf(b)), 
        //     A.prototype);
        // assert.strictEqual(
        //     Reflect.getPrototypeOf(
        //         Reflect.getPrototypeOf(
        //             Reflect.getPrototypeOf(b))), 
        //     Object.prototype);
    });
});