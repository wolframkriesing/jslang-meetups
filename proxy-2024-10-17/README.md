# Proxy - #jslang 76th Edition

At Check24

🎉 🎉 🎉 we moved to https://lu.ma/jslang 🎉 🎉 🎉  

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what is Proxy?
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn by writing a test

- [ ] Can I find out if an object is proxied?
- [ ] Can I proxy a function?
- [x] Are you mutating the original object?
- [ ] Can I unproxy a proxied object?
- [ ] Can you chain proxies?
- [x] Can a proxy setter/getter be async?



## Topic for the next meetup

   arguments
   null vs undefined
   Object.groupBy




## How we ran the meetup?

We organized the meetup on [meetup].

[meetup]: https://www.meetup.com/JavaScript-The-Language
