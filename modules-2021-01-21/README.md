# JavaScript Modules

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
       Repo: https://codeberg.org/wolframkriesing/jslang-meetups
2) [x] Introduce each other
3) [x] Quickly align on what are "JavaScript Modules" 
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [x] Choose next meetup topic
7) [x] Hangout ...

## Collect what we want to learn

The questions we would like to answer today:

1) [ ] How importing modules differs from import a file?
2) [ ] What is the parent scopes / surrounding scopes?
3) [ ] Meta data for a modules?
4) [ ] What sequence of events happening when importing?
5) [ ] How does it relate to "use strict"?
6) [ ] Can we import from a 3rd party library?
7) [ ] Lazy loading, dynamic import?
8) [ ] Can we intercept an import? If so how?
9) [ ] How to use `export default`?



## Topic for the next meetup

- 7 promises in javascript
- 3 this
-   modules
- 1 generators
- 4 === 
-   array methods
-   undefined
- 2 BigInt


## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language

## References
- Joerns experiment with data URLs https://gist.github.com/Narigo/670e39d7d6d456db9c5e07ae78b7adc7
- Module Imports spec chapter https://tc39.es/ecma262/#sec-imports

- The tc39 page https://tc39.es/
- Practice JavaScript online
- This source code and other meetup's source https://gitlab.com/wolframkriesing/jslang-meetups