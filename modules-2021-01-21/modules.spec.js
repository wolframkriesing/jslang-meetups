// Source at: https://codeberg.org/wolframkriesing/jslang-meetups
import {strict as assert} from 'assert';

import {something} from './thing.js';

// Error [ERR_UNSUPPORTED_ESM_URL_SCHEME]: Only file and data URLs are supported by the default ESM loader. Received protocol 'https:'
// import somethingFromTheWeb from 'https://codeberg.org/wolframkriesing/jslang-meetups/raw/branch/main/top-level-await-2020-12-17/fast.js';

// data:[<MIME-Typ>][;charset=<Zeichensatz>][;base64],<Daten>
// import somethingFromTheWeb from 'data:;base64,ZXhwb3J0IGRlZmF1bHQgJ2Zhc3QtZmlsZSc7';
// import somethingFromTheWeb from 'data:text/plain;charset=utf-8;base64,ZXhwb3J0IGRlZmF1bHQgJ2Zhc3QtZmlsZSc7';
// import somethingFromTheWeb from 'data:,export default "fast-file";';

describe('JS Modules - files vs. modules', () => {
  const importedThing = something;
  it('import "thing" from a file, is equal to exported "thing" from file', () => {
    assert.equal('thing', importedThing);
  });

  it('import inline fails with SyntaxError, because "Cannot use import statement outside a module"', () => {
    assert.throws(() => {
      eval("import {something} from './thing.js';")
    }, SyntaxError);
  });

  xit('import something that is not exported will throw SyntaxError', () => {
    // we dont know how to write a test for it
  });

  xit('import works via http', () => {
    assert.equal('fast-file', somethingFromTheWeb);
  });

});
