# Tagged Templates

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what tagged templates is supposed to be 
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [x] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

The questions we would like to answer today:

1) [x] What's the difference to a function?
2) [x] What's the arguments that the `fn` gets?
3) [x] Can I reuse other functions?
4) [x] What's the function interface?
5) [x] Can multiple functions be combined?
6) [ ] Can we use an async function?
7) [ ] How does it behave when the fn throws?
8) [ ] What's the scope of the variables passed to them?
9) [ ] Can you use generator functions?



## Topic for the next meetup

- 2  ?? nullish coal....                          
- 3  numb3rs: 111n, 0111, 0x111, 111_222_333     
- 2  (Shared)ArrayBuffer                       
- 5  top level await
- 1  WeakMap, WeakSet
-    weak reference
- 3  Map, Set


## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language

## References

- MDN page https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Optional_chaining
- ECMAScript Spec as used in the meetup
  - https://tc39.es/ecma262/#prod-OptionalChain

- The tc39 page https://tc39.es/
- Practice JavaScript online https://jskatas.org/#bundle-es6-katas
- This source code and other meetup's source https://gitlab.com/wolframkriesing/jslang-meetups