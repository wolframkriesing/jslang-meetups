# Reflect

A topic that we all rarely use. Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) Introduce each other
3) Quickly align on what Reflect is
4) Collect what we want to learn 
5) Drive the code, together
6) Choose next meetup topic
7) Hangout ...

## Collect what we want to learn

The questions we would like to answer today:

1) [x] Difference between Object and Reflect
2) [x] Create an object using Reflect
3) [x] Override properties using Reflect
4) [x] Creating a class instance
5) [ ] Reflect.has('x', obj) vs. 'x' in obj
6) [ ] Reflecting on Reflect
7) [ ] Reflecting static object, like `Math`
8) [x] `Reflect.construct`
9) [ ] is a frozen object configurable=false?

## Topic for the next meetup

- dot operator      ......
- BigInt            ....
- optional chaining ...
- closures          



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language/events/269302819/

## References

- MDN page https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect
- ECMAScript Spec as used in the meetup
  - The `Reflect` section https://tc39.es/ecma262/#sec-reflect-object
  - The `Object` section https://tc39.es/ecma262/#sec-object-constructor

- The tc39 page https://tc39.es/
- Practice JavaScript online https://jskatas.org/#bundle-es6-katas
- This source code and other meetup's source https://gitlab.com/wolframkriesing/jslang-meetups