# dynamic import()

## The plan

The plan for the meetup was to:

1. Collect items that we want to learn about dynamic import()
1. Write tests to learn, verify and describe some type conversion properties. 

## How we ran the meetup

We organized the meetup on [meetup.com][meetup].

## Onsite

We started by introducing each other shortly.
Then we did 1. and collected items.

## Recap

TBD
