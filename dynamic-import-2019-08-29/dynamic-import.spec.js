import assert from 'assert';
import staticMocha from 'mocha';

describe('How to use dynamic imports', () => {
  it('calling `import(assert)` returns Promise', () => {
    assert(import('assert') instanceof Promise);
  });
  it('calling import with a non-existent thingy, rejects', async () => {
    await assert.rejects(import('non-existent'));
  });
});

describe('Relative and absolute paths', () => {
  it('imports relative empty local JS file', async () => {
    await assert.doesNotReject(import('./empty-file.js'));
  });
  it('importing an empty file resolves to `undefined`', async () => {
    const emptyFile = await import('./empty-file.js');
    assert.deepEqual(emptyFile.default, undefined);
  });
// commented out, didnt get it to pass ... not sure what the spec says
  xit('a relative directory imports the index.js', async () => {
    const directory = await import('./');
    assert.deepEqual(directory.default, {index: 'js'});
  });
  it('import resolves a node_modules module', async () => {
    const mocha = await import('mocha');
    assert(mocha.Suite);
  });
  // commented out, not sure how to re-enable
  xit('importing the same file using resolved path+relative path, is the same', async () => {
    const resolvedMocha = await import('mocha');
    const relativeMocha = await import('../node_modules/mocha');
    assert.strictEqual(resolvedMocha, relativeMocha);
  });
  it('static and dynamic imports are the same', async () => {
    const resolvedMocha = await import('mocha');
    assert.strictEqual(staticMocha, resolvedMocha.default);
  });
});

describe('Global scope and dir', () => {
  it('an imported file can modify `globalThis.variable`', async () => {
    globalThis.variable = 1;
    await import('./global-that.js');
    assert.strictEqual(globalThis.variable, 2);
  });
  it('imported file can delete property `globalThis.toBeDeleted`', async () => {
    globalThis.toBeDeleted = 'irrelevant value';
    await import('./deleting-global-prop.js');
    assert.strictEqual(Object.hasOwnProperty(globalThis, 'toBeDeleted'), false);
  });
  it('importing the same file twice (with `?1` suffix) DOES modify the same variable twice (cache buster works)', async () => {
    globalThis.variable = 1;
    await import('./global-twice.js');
    await import('./global-twice.js?1');
    assert.strictEqual(globalThis.variable, 3);
  });
  it('importing using cache buster, returns NOT the same object', async () => {
    const first = await import('./global-twice.js');
    const second = await import('./global-twice.js?1');
    assert.notStrictEqual(first, second);
  });
});

describe('Nested imports', () => {
  it('not cache-busted indirect imports return the same each', async () => {
    const import1 = await import('./root.js');
    const import2 = await import('./root.js?');
    assert.strictEqual(import1.default, import2.default);
  });
});
