# AsyncGenerators

Let's figure out what we can learn about it.

## Agenda

Onsite: 🍏 🍕 🥤 by Collaboration Factory AG https://www.collaboration-factory.de
Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are async-generators
4) [x] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] what happens when you mix yielded types (static value and promise)?
- [x] for await - how does it work? what kind of syntactic sugar is it?
- [ ] race conditions?
- [ ] can you generate async generators from them
- [ ] compare async-gen to Promise.all(), Promise.race()
- [x] yield inside a setTimeout?



## Topic for the next meetup

    immediately resolved Promises
    passing values to next() in generators
6   regular expression object
    what is the latest features in ES
    



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
