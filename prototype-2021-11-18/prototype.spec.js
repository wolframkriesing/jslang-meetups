import {strict as assert} from 'assert';

describe('what in JS has (no) prototype?', () => {
  it('object-literal has a prototype', () => {
    const prototypeOf = Reflect.getPrototypeOf({});
    assert.equal(prototypeOf, Object.prototype);
  });
  it('undefined has NO prototype', () => {
    assert.throws(() => {
      Reflect.getPrototypeOf(undefined);
    }, TypeError);
  });
});

describe('type coercion ...', () => {
  it('double equal comparison of null and undefined returns true', () => {
    assert.equal(undefined == null, true);
    assert.equal(undefined != null, false);
  });
  xit('undefined and null converted to a string are equal', () => {
    assert(String(null) === String(undefined));
  });
  it('converting undefined+null to Boolean makes them equal', () => {
    assert(Boolean(null) === Boolean(undefined));
  });
  it('triple equal compare of null and undefined return false', () => {
    assert.equal(undefined === null, false);
  });
});

describe('class is syntactic sugar?', () => {
  it('the prototype of a base class IS NOT the same as the prototype of the extending class', () => {
    class Base {}
    class MyClass extends Base {}
    assert.notEqual(Reflect.getPrototypeOf(MyClass), Reflect.getPrototypeOf(Base));
  });
  it('the prototype of an object is of type object', () => {
    assert.equal(typeof Reflect.getPrototypeOf({}), 'object');
  });
  it('the top of the prototype chain is null', () => {
    const objectsProto = Reflect.getPrototypeOf(Object);
    assert.equal(Reflect.getPrototypeOf(Reflect.getPrototypeOf(objectsProto)), null);
    assert.equal(Reflect.getPrototypeOf(Reflect.getPrototypeOf({})), null);
  });
  it('circular prototype chain is possible', () => {
    const obj1 = {};
    obj1.prototype = obj1;
    assert.equal(obj1.prototype, obj1);
    assert.equal(obj1.foo, undefined);
    assert.equal(obj1.prototype.prototype.prototype.prototype.prototype.prototype, obj1.prototype);
  });
  it('is prototype same as __proto__ - NO, __proto__ is GONE', () => {
    const obj = {};
    assert.notEqual(obj.prototype, obj.__proto__);
  });
});
