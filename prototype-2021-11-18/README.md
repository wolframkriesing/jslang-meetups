# prototype

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what prototype is
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [x] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [ ] how to create custom error types (extending Error)
- [ ] class is syntactic sugar?
- [ ] prototype is just an object, blue print
- [ ] circular prototype chain
- [ ] object, constructor, function, prototype
- [ ] prototype on an object instance, can u change it?
- [x] what in JS has no prototype?


The questions we would like to answer today:

1) [ ] 
2) [ ] 
3) [ ] 


## Topic for the next meetup




## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
