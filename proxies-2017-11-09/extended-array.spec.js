import assert from 'assert';

describe('Proxy, for arrays', () => {

  const proxiedArray = (arr) => new Proxy(arr, {
    get: (target, index) => {
      const n = index == 0 ? 0 : Math.abs(target.length);
      const newIndex = (+index+target.length*n) % target.length;
      return target[newIndex];
    },
    set: (target, index, value) => {
      if (target.length === 0) {
        target[0] = value;
        return true;
      }
      const n = index == 0 ? 0 : Math.abs(target.length);
      const newIndex = (+index+target.length*n) % target.length;
      target[newIndex] = value;
      return true;
    }
  });

  it('for index -1, returns last element of a one element array', () => {
    const arr = ['last'];
    assert.equal(proxiedArray(arr)[-1], 'last');
  });
  it('for index -1, returns last element of a two elements array', () => {
    const arr = ['1st', '2nd'];
    assert.equal(proxiedArray(arr)[-1], '2nd');
  });
  it('for index 0, returns first element of a two elements array', () => {
    const arr = ['1st', '2nd'];
    assert.equal(proxiedArray(arr)[0], '1st');
  });
  it('for index 2, returns last element of a three elements array', () => {
    const arr = proxiedArray(['1st', '2nd', '3rd']);
    assert.equal(arr[2], '3rd');
  });
  it('for index -2, returns first element of a two elements array', () => {
    const arr = ['1st', '2nd'];
    assert.equal(proxiedArray(arr)[-2], '1st');
  });
  it('access 3rd element of a two-element array, returns 1st element', () => {
    const arr = ['1st', '2nd'];
    assert.equal(proxiedArray(arr)[2], '1st');
  });
  it('access any element of an empty array return undefined', () => {
    assert.equal(proxiedArray([])[-1], undefined);
  });
  it('setting an element still works as expected', () => {
    const arr = [];
    arr[1] = 'one';
    assert.equal(arr[1], 'one');
  })
  it('setting an element on a -1 index, override the last element', () => {
    const arr = proxiedArray(['1st', '2nd', '3rd']);
    arr[-1] = 'three';
    assert.equal(arr[0], '1st');
    assert.equal(arr[1], '2nd');
    assert.equal(arr[2], 'three');
  })

  it('setting an element on index 0 , override the first element', () => {
    const arr = proxiedArray(['1st', '2nd', '3rd']);
    arr[0] = 'one';
    assert.equal(arr[0], 'one');
    assert.equal(arr[1], '2nd');
    assert.equal(arr[2], '3rd');
  })

    xit('setting an element on index -1 on empty array', () => {
    const arr = proxiedArray([]);
    arr[-1] = 'one';
    console.log('sss',arr.map)

    assert.deepEqual([...arr], ['one']);
  })

});

describe('proxy functions', () => {
  it('proxy functions stays callable', () => {
    const myFunc = () => 42;
    const myProxy = new Proxy(Set, {});
// console.log(myProxy)
//     assert.equal(myProxy(), 42)
//     assert.equal(myProxy instanceof Proxy, true)
  })
})

// arr = [0,1,2]
// arr[-1] => 2
