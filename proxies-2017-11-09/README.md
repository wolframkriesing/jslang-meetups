# Proxies

First we tried to gain a common understanding of what proxies can be used for.
Than we collected a couple of use cases where we could use them.
What we found were the following cases:

1. `arr = [0,1,2]`
   `arr[-1] => 2`
2. `x = {}`
   `x.y.z => undefined`
3. `arr = [0..10]`
   `[0,1,2,3…,10]`
   we could not come up with a valid JS syntax
4. `“one”.substring(-2)`
   `“ne”`
5. `[1,2,3].split(-1)`
   `3`
6. `print(1)`
   `“2017-11-10 20:10 1”`
7. `observerAllObejct()`
   `Object.instances`
8. proxy `describe, it`
   build a custom test reporter

## How we ran the meetup

We organized the meetup on [meetup.com].

In the end we implemented the [test cases and the production code][impl].
First we had some discussions on TDD, but soon focused quickly on moving the 
functionality forward. 

The biggest finding was that proxying an array makes the array become an object
and not an array anymore, it becomes a Proxy instance, where we had hoped it stays
an array.
 
[impl]: ./extended-array.spec.js
[meetup.com]: https://www.meetup.com/JavaScript-The-Language/events/243955512