# Generators - #jslang 69th Edition

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are generators
4) [x] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [ ] can we have nested generators?
- [x] how to start and pause a generator?
- [x] take a string and yield every character
- [x] can i have multiple `yield`s inside a generator?
- [x] Can I return from within a generator?
- [ ] Passing a function
- [ ] 




## Topic for the next meetup

The prototype




## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
