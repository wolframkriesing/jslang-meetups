import assert from 'assert';

xit('weird', () => {
	  assert.strictEqual(undefined==false, true);
});
it('ToBoolean: undefined ', () => {
	  const undefinedAsBool = !!undefined;
	  assert.strictEqual(undefinedAsBool, false);
});
it('ToBoolean: undefined, using `Boolean`', () => {
	  const undefinedAsBool = Boolean(undefined);
	  assert.strictEqual(undefinedAsBool, false);
});
it('ToBoolean: null, using `Boolean`', () => {
	  const nullAsBool = Boolean(null);
	  assert.strictEqual(nullAsBool, false);
});
it('ToBoolean: +0, using `Boolean`', () => {
	  const asBool = Boolean(+0);
	  assert.strictEqual(asBool, false);
});
it('ToBoolean: -0, using `Boolean`', () => {
	  const asBool = Boolean(-0);
	  assert.strictEqual(asBool, false);
});
it('ToBoolean: NaN, using `Boolean`', () => {
	  const asBool = Boolean(NaN);
	  assert.strictEqual(asBool, false);
});
it('ToBoolean: Infinity, using `Boolean`', () => {
	  const asBool = Boolean(Infinity);
	  assert.strictEqual(asBool, true);
});
it('ToBoolean: -Infinity, using `Boolean`', () => {
	  const asBool = Boolean(-Infinity);
	  assert.strictEqual(asBool, true);
});
it('ToBoolean: Number.EPSILON, using `Boolean`', () => {
	  const asBool = Boolean(Number.EPSILON);
	  assert.strictEqual(asBool, true);
});
it('ToBoolean: Number.EPSILON/2, using `Boolean`', () => {
	  const asBool = Boolean(Number.EPSILON/2);
	  assert.strictEqual(asBool, true);
});
it('ToBoolean: empty string, using `Boolean`', () => {
	  const asBool = Boolean('');
	  assert.strictEqual(asBool, false);
});
it('ToBoolean: empty string, using `Boolean`', () => {
	  const asBool = Boolean('0');
	  assert.strictEqual(asBool, true);
});
it('ToBoolean: empty string, using `Boolean`', () => {
	  const asBool = Boolean(''+'');
	  assert.strictEqual(asBool, false);
});
it('ToBoolean: {}, using `Boolean`', () => {
	  const asBool = Boolean({});
	  assert.strictEqual(asBool, true);
});
it('ToBoolean: Boolean, using `Boolean`', () => {
	  const asBool = Boolean(Boolean);
	  assert.strictEqual(asBool, true);
});
it('ToBoolean: Number.MIN_VALUE, using `Boolean`', () => {
	  const asBool = Boolean(Number.MIN_VALUE);
	  assert.strictEqual(asBool, true);
});
it('ToBoolean: Number.MIN_VALUE/2, using `Boolean`', () => {
	  const asBool = Boolean(Number.MIN_VALUE/2);
	  assert.strictEqual(asBool, false);
});
it('ToBoolean: Number.MIN_VALUE-Number.MIN_VALUE, using `Boolean`', () => {
	  const asBool = Boolean(Number.MIN_VALUE-Number.MIN_VALUE);
	  assert.strictEqual(asBool, false);
});

