import assert from 'assert';

describe('to number', () => {
  it('ToNumber: undefined, using `Number`', () => {
      const asNumber = Number(undefined);
      assert.strictEqual(Number.isNaN(asNumber), true);
  });
  it('ToNumber: whitespace, using `Number`', () => {
      const asNumber = Number(' ');
      assert.strictEqual(asNumber, 0);
  });
  it('ToNumber: TAB, using `Number`', () => {
      const asNumber = Number("\t");
      assert.strictEqual(asNumber, 0);
  });
  it('ToNumber: \\n, using `Number`', () => {
      const asNumber = Number("\n");
      assert.strictEqual(asNumber, 0);
  });
  it('ToNumber: TAB\\n, using `Number`', () => {
      const asNumber = Number("\t\n");
      assert.strictEqual(asNumber, 0);
  });
  it('ToNumber: +.TAB, using `Number`', () => {
      const asNumber = Number("+.\t");
      assert.strictEqual(isNaN(asNumber), true);
  });
  it('ToNumber: +.0TAB, using `Number`', () => {
      const asNumber = Number("+.0\t");
      assert.strictEqual(asNumber, 0);
  });
  it('ToNumber: 0a, using `Number`', () => {
      const asNumber = Number("0a");
      assert.strictEqual(isNaN(asNumber), true);
  });
  it('ToNumber: 0xa, using `Number`', () => {
      const asNumber = Number("0xa");
      assert.strictEqual(asNumber, 0xA);
  });
  it('ToNumber: `0 a`, using `Number`', () => {
      const asNumber = Number("0 a");
      assert.strictEqual(isNaN(asNumber), true);
  });
  it('ToNumber: 0xa, using `Number`, compared to 10', () => {
      const asNumber = Number("0xa");
      assert.strictEqual(asNumber, 10);
  });
  it('ToNumber: 012, using `Number`, compared to 10', () => {
      const asNumber = Number("012");
      assert.strictEqual(asNumber, 12);
  });
  it('ToNumber: 0000012, using `Number`, compared to 10', () => {
      const asNumber = Number("0000012");
      assert.strictEqual(asNumber, 12);
  });
  it('ToNumber: 00000.12, using `Number`, compared to 10', () => {
      const asNumber = Number("00000.12");
      assert.strictEqual(asNumber, .12);
  });
  
  
  // -!0
  it('ToBoolean: -!0', () => {
      const asBool = Boolean(-!0);
      assert.strictEqual(asBool, true);
  });
  it('ToBoolean: -true', () => {
      const minusTrue = -true;
      assert.strictEqual(minusTrue, -1);
  });
  
  it('isNaN works????', () => {
      assert.strictEqual(isNaN("JSLANG"), true);
  });
  it('isNaN works???? #', () => {
      assert.strictEqual(Number.isNaN("JSLANG"), false);
  });
  
  it('toString, 1', () => {
      const toS = '' + {
            toString: () => '1', 
            valueOf: () => '2'
          };
      assert.strictEqual(toS, '2');
  });
  it('toString, 2, `String`', () => {
      const toS = String({
            toString: () => '1', 
            valueOf: () => '2'
          });
      assert.strictEqual(toS, '1');
  });
  it('toString, 2, `Number`', () => {
      const toS = Number({
            toString: () => '1', 
            valueOf: () => '2'
          });
      assert.strictEqual(toS, 2);
  });
});