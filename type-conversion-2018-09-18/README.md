# Type Conversion

## The plan

The plan for the meetup was to:

1. Collect items that we want to learn about type conversion
1. Write tests to learn, verify and describe some type conversion properties. 

## How we ran the meetup

We organized the meetup on [meetup.com][meetup].

## Onsite

We started by introducing each other shortly.
Then we did 1. and collected items. We came up with this:
* spec
* edge cases
* NaN
* useful sides of it
* TypedArrays
* Error
* compare objects
* error prevention

We started reading the [spec][to-bool] especially the "ToBoolean" section, which describes
the rules that apply when converting any type to a boolean. We had to decide first
how to write a test to convert something to a boolean, after we had collected a couple
ideas we deicded to use `Boolean()` for it. And then we wrote the [according tests][to-bool-tests].

The next chapter in the spec was ["ToNumber"][to-number] which we went over and wrote
[tests][to-number-tests] for every feature described there. While working on this part
we learned a lot about `isNaN`, `Number.isNaN`, hexadecimal numbers and much more.
We had one great example, which helped us learn the BNF form 

All the way at the end I jumped to the ["[[DefaultValue]]"][all-spec-versions] chapter in the 
very first spec (it doesnt exist in the latest one anymore).
I would have liked to have gotten there while working on our tests, but somehow I had
missed the right point in time. In this section of the spec it was described
how `toString` and `valueOf` work and when they apply.

Which test to write and how to solve it was always done in the group. People raised 
suggestions for the next test to write, I tried to coordinate all this a bit and hope
we covered lots of interesting things.

[to-number-tests]: ./to-number.spec.js
[to-bool-tests]: ./to-bool.spec.js
[to-bool]: https://tc39.github.io/ecma262/#sec-toboolean
[to-number]: https://tc39.github.io/ecma262/#sec-tonumber
[spec]: https://tc39.github.io/ecma262/#sec-type-conversion
[meetup]: https://www.meetup.com/JavaScript-The-Language/events/251525280 
[all-spec-versions]: https://www.ecma-international.org/publications/standards/Ecma-262-arch.htm
