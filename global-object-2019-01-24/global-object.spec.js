import assert from 'assert';

const globalObject = Function('return this')();

describe('Access the global object', () => {
  it('it has all 3 properties, when using `Function`', () => {
    // const globalObject = (new Function('return this'))();
    
    assert(Object.getOwnPropertyNames(globalObject).includes('Infinity'));
    assert(Object.getOwnPropertyNames(globalObject).includes('NaN'));
    assert(Object.getOwnPropertyNames(globalObject).includes('undefined'));
  });
  it('is NaN coerced to false?', () => {
    assert(Boolean(NaN) == false);
    assert(!!NaN == false);
  });
  it('is NaN falsey', () => {
    assert.notEqual(NaN, true);
  });
  it('IIFE returning `this` returns undefined', () => {
    const resultIIFE = (function() { return this; })();
    assert.equal(resultIIFE, undefined);
  });
});

describe('Inspect the Global Object', () => {
  it('looks like an object', () => {
    assert.equal(typeof globalObject, 'object');
  });
  it('is an instanceof an "Object"', () => {
    assert(globalObject instanceof Object);
  });
  it('has at least 3 + 9 properties', () => {
    assert(Object.getOwnPropertyNames(globalObject).length > 3+9);
  });
  it('Infinity is global', () => {
    assert.strictEqual(Infinity, globalObject.Infinity);
  });
  it('Object is global', () => {
    assert.strictEqual(Object, globalObject.Object);
  });
});

describe('Delete from Global Object', () => {
  it('`NaN`, does throw', () => {
    assert.throws(() => {
      delete globalObject.NaN;
    });
  });
  
  // running the following screws up any environment, because Object 
  // gets really removed :(
  // it('`Object`, does not throw', () => {
  //   assert.throws(() => {
  //     delete globalObject.Object;
  //   });
  // });
  
  // running the following, they do pass, but screw up the global object, eval in this case 
  // and make other tests fail
  // it('`eval`, does NOT throw', () => {
  //   assert.doesNotThrow(() => {
  //     delete globalObject.eval;
  //   });
  // });
  // it('`eval`, does NOT throw and `eval` is GONE', () => {
  //   delete globalObject.eval;
  //   assert.equal(Object.getOwnPropertyDescriptor(globalObject, 'eval'), undefined);
  // });
  
  // "Value Properties" have property descriptors in the spec but 
  // "Function Properties" don't have property descriptors
  
});

it('change property descriptor of `NaN`', () => {
  assert.throws(() => {
    Object.defineProperty(globalObject, 'NaN', {writable: true, configurable: true, enumerable: true});
  });
});

