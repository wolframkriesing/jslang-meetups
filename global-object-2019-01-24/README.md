# The Global Object

## The plan

The plan for the meetup was to:

1. Collect items that we want to learn about type conversion
1. Write tests to learn, verify and describe some type conversion properties. 

## How we ran the meetup

We organized the meetup on [meetup.com][meetup].

## Onsite

We started by introducing each other shortly.
Then we did 1. and collected items. We came up with this:
* how to access the global object
* properties of it
* inspect the global object
* `this`
* the global scope
* Intl

## Recap

This time it was quite interesting, since we worked on the Global Object,
and once we modified it, we actually had quite some side-effects.
Not only that `eval` once we had removed it from the Global Object, which
was possible, could not be found in the next test anymore, of course.
But also once we remove `Object` from the Global Object we screwed up
a lot of things, even WebStorm started complaining :). That was fun.
See all the commented out tests and also the reasoning why we commented
them out, even though the were passing.

[meetup]: https://www.meetup.com/JavaScript-The-Language/events/258081106/