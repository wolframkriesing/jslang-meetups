import assert from 'assert';

describe('what does define/deleteProperty do?', () => {
  it('add property `Reflect.defineProperty` with value 42, allows to read it', () => {
    const obj = {};
    Reflect.defineProperty(obj, 'test', {value: 42});
    assert.strictEqual(obj.test, 42);
  });
  it('Reflect.defineProperty on an object where the property already exists, will override it', () => {
    const obj = {test: 42};
    Reflect.defineProperty(obj, 'test', {value: 41});
    assert.strictEqual(obj.test, 41);
  });
  it('Reflect.defineProperty returns false when used on a frozen object', () => {
    const obj = Object.freeze({test: 42});
    const result = Reflect.defineProperty(obj, 'test', {value: 41});
    assert.strictEqual(result, false);
  });
  it('setting the value the property has, again, on a frozen object, returns true (as if it worked)', () => {
    const obj = Object.freeze({test: 42});
    const result = Reflect.defineProperty(obj, 'test', {value: 42});
    assert.strictEqual(result, true);
  });
});

describe('we can not unfreeze a frozen object using Reflect', () => {
  it('can NOT set a property of a frozen object by setting writable=true and configurable=true in the same call', () => {
    const obj = Object.freeze({test: 42});
    Reflect.defineProperty(obj, 'test', {
      value: 41, writable: true, configurable: true});
    assert.strictEqual(obj.test, 42);
  });
  it('can NOT set a property of a frozen object by setting writable=true, configurable=true in a call before setting a value', () => {
    const obj = Object.freeze({test: 42});
    Reflect.defineProperty(obj, 'test', {writable: true, configurable: true});
    Reflect.defineProperty(obj, 'test', {value: 41});
    assert.strictEqual(obj.test, 42);
  });
  it('can NOT set a property of a frozen object by setting configurable=true and writable=true in separate calls before setting the value', () => {
    const obj = Object.freeze({test: 42});
    Reflect.defineProperty(obj, 'test', {configurable: true});
    Reflect.defineProperty(obj, 'test', {writable: true});
    Reflect.defineProperty(obj, 'test', {value: 41});
    assert.strictEqual(obj.test, 42);
  });
});

describe('Trying to unfreeze an object', () => {
  it('the property descriptor of the property of a frozen object', () => {
    const obj = Object.freeze({test: 42});
    assert.deepEqual(Reflect.getOwnPropertyDescriptor(obj, 'test'), {
      configurable: false,
      enumerable: true,
      value: 42,
      writable: false
    });
  });
  it('Reflect.isExtensible on a frozen object is false', () => {
    const obj = Object.freeze({test: 42});
    assert.strictEqual(Reflect.isExtensible(obj), false);    
  });
});