# `Reflect` - #jslang 74th Edition

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what is `Reflect`
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn by writing a test

- [ ] can we define properties on global/frozen objects? can we override Math.min()?
- [ ] what does define/deleteProperty do?
- [ ] can we do an instanceof using Reflect?
- [ ] if an obj is NOT extensible can I override each prop?
- [ ] 



## Topic for the next meetup

4  proxy
1  generators
4  async generators
1  destructuring






## How we ran the meetup?

We organized the meetup on [meetup].

[meetup]: https://www.meetup.com/JavaScript-The-Language
