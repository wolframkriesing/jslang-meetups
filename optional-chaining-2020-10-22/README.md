# Optional Chaining (?.)

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what optional chaining is supposed to be
4) [ ] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

The questions we would like to answer today:

1) [x] What's the default return value if an element is undefined/null? (without the operator)
2) [ ] How does it work with (async, generators) functions?
3) [x] What about newlines, spaces?
4) [ ] Can it throw an exception?
5) [ ] How does it behave with getters?
6) [ ] How does it work under the hood?
7) [ ] Return type?



## Topic for the next meetup

- ?? nullish coalis***    111
- Promises                 
- Object (ES6+ features)  1
- getter and setter       111
- tagged templates        1111



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language

## References

- MDN page https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Optional_chaining
- ECMAScript Spec as used in the meetup
  - https://tc39.es/ecma262/#prod-OptionalChain

- The tc39 page https://tc39.es/
- Practice JavaScript online https://jskatas.org/#bundle-es6-katas
- This source code and other meetup's source https://gitlab.com/wolframkriesing/jslang-meetups