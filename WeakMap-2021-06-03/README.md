# WeakMap

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what WeakMap is
4) [ ] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

The questions we would like to answer today:

1) [x] Many refs to an object, remove one, its still accessible?
2) [x] What if the ref is stored in a new variable? And the old one removed?
3) [x] ~~Push two refs into an array, change the array - WeakMap is gc'ed?~~ we can not verify that, see tests :(
4) [x] Change the ref object, e.g. add a field, remove etc.
5) [x] WeakMap as a key for a WeakMap?
6) [ ] Can we use value of one WeakMap as key for another?
7) [ ] Using global vars as key. (but overridable, like window, document, ...)
8) [ ] v8.getHeapStatistics to see if memory is freed up
9) [ ] screw around with the gc? be bad


## Topic for the next meetup

- 1 bind, call, apply
- 1 `new`
- 5 !==, ===, ==, != ...
-   Map
-   top level await
- 3 modules
- 2 type coercion



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language

## References

- MDN page https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap
- ECMAScript Spec as used in the meetup
  - https://tc39.es/ecma262/#sec-weakmap-constructor

- This source code and other meetup's source https://gitlab.com/wolframkriesing/jslang-meetups