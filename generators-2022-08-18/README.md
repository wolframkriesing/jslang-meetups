# Generators

Let's figure out what we can learn about it.

## Agenda

Onsite: 🍏 🍕 🥤 by Alasco https://www.alasco.de/
Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are generators
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [x] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] what is a generator  ~~/iterator~~
- [ ] can you create an anonymous generator?
- [ ] can you call `new` on the generator function?
- [ ] what is the execution flow
- [ ] what happens with async/await
- [ ] what happens if a generator is abandoned? (what happens to the ref count?)
- [ ] what happens if a generator has ended?
- [ ] can you extend the generator protoype? (add new methods to the prototype)
- [ ] can you restart a generator?



## Topic for the next meetup
1 object methods like: toJSON, call, assign, ... 2
2 async generators 3
3 Reflect  2
4 Proxies  4




## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
