# closure - #jslang 73rd Edition (the alternative to the soccer game – edition)

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what is a closure
4) [x] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn by writing a test

- [x] is there a definition for "closure"?
- [x] how far can we nest a closure?
- [x] block `{ }` has the same rules like closures?
- [x] `var` vs. `let` in a closure?
- [x] dynamically created closures



## Topic for the next meetup

   context vs. environment (as defined in ECMAScript spec)
   scopes (e.g. lexical, ...)
   Reflect



## How we ran the meetup?

We organized the meetup on [meetup].

[meetup]: https://www.meetup.com/JavaScript-The-Language
