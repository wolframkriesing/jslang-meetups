import assert from 'assert';

describe('dot operator syntax and primitive types', () => {
  it('`1.` indicates a number and `1.toString()` is no valid JS', () => {
    assert.throws(() => eval('1.toString()'), SyntaxError);
  });
  it('`(1).toString()` is valid JS', () => {
    assert.doesNotThrow(() => eval('(1).toString()'));
  });
  it('`().toString()` is NOT valid JS', () => {
    assert.throws(() => eval('().toString()'), SyntaxError);
  });
  it('`(undefined).toString()` throws because there is no `toString()` on undefined', () => {
    assert.throws(() => eval('(undefined).toString()'), TypeError);
  });

  it('`1..toString()` evaluates to "1"', () => {
    assert.strictEqual(1..toString(), '1');
  });
  it('`1..toString() === `1.0.toString()` equal?', () => {
    assert.strictEqual(1..toString(), 1.0.toString());
  });

  it('`(1.).toString()` equals `1..toString()`', () => {
    assert.strictEqual((1.).toString(), 1..toString());
  });
  it('dot operator on a boolean', () => {
    assert.strictEqual(true.toString(), 'true');
  });
  it('double dot on booleans do NOT work', () => {
    assert.throws(() => eval('true..toString()'), SyntaxError);
  });
  it('a float toString() works???', () => {
    assert.doesNotThrow(() => eval('1.2.toString()'));
    assert.strictEqual(1.2.toString(), '1.2');
  });

  it('`((1.).5).toString` ...', () => {
    assert.throws(() => eval('((1.).5).toString'), SyntaxError);
  });
});
