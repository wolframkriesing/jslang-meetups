# Dot Operator

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) Introduce each other
3) Quickly align on what dot operator is supposed to be
4) Collect what we want to learn 
5) Drive the code, together
6) Choose next meetup topic
7) Hangout ...

## Collect what we want to learn

The questions we would like to answer today:

1) [x] Why does `1..toString()` work, why not `1.toString()`?
2) [x] `1.2.toString()` ???
3) [ ] diff between false, undefined, null with `false?.`
4) [ ] optional chaining works with brackets? e.g. `obj?.['prop']`
5) [ ] is there a dot-operator?
6) [ ] optional chaining with functions, e.g. `f?.()`

## Topic for the next meetup

- exotic object       ..
- optional chaining   ....
- null coalescing     


## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language/events/269302819/

## References

- MDN page ...
- ECMAScript Spec as used in the meetup
  - ....

- The tc39 page https://tc39.es/
- Practice JavaScript online https://jskatas.org/#bundle-es6-katas
- This source code and other meetup's source https://gitlab.com/wolframkriesing/jslang-meetups