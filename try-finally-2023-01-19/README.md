# try-finally

Let's figure out what we can learn about it.

## Agenda

Onsite: 🍏 🍕 🥤 by **TNG** https://www.tngtech.com/  
Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are try+finally
4) [x] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] Use try+finally without catch?
- [ ] When was `finally` introduced into ECMAScript, and the motivation/reason?
- [x] How is the execution order of the blocks, try, finally?
- [x] What happens when a block returns?
- [ ] Is there something to throw that prevents finally from being run?
- [ ] Does using async/await inside finally change its behavior?
- [ ] Do try+finally share their lexical scope?



## Topic for the next meetup

1   with
    try+catch+finally
2   var, let, const
1   Proxies
4   labels
6   Map, Set, WeakMap, ...



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
