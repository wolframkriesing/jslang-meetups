# async generators - #jslang 75th Edition

At foobar Agency

🎉 🎉 🎉 we moved to https://lu.ma/jslang 🎉 🎉 🎉  

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what async generators?
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [x] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn by writing a test

- [x] use an empty async generator
- [x] does it wait until it has the next data item?
- [ ] does it throw an error when the Promise fails?
- [ ] how do we catch errors?
- [ ] what is the order it yields the values?
- [x] simulate slow data using setTimeout
- [ ] will terminate early with return



## Topic for the next meetup

7  nesting Promise
4  hoisting
9  Proxy
7  with statement



## How we ran the meetup?

We organized the meetup on [meetup].

[meetup]: https://www.meetup.com/JavaScript-The-Language
