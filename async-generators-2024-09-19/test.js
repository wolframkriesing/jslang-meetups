import assert from 'assert';

describe('use an empty async generator', () => {
  it('calling next() on an async generator returns a promise', () => {
    // arrange 
    async function* emptyGenerator() {}

    // act
    const result = emptyGenerator().next();
    // assert
    assert.strictEqual(result instanceof Promise, true);
  });

  describe('an async generator resolve to an object with properties', () => {
    it('done=true', async () => {
      async function* generator() {}

      const result = await generator().next();
      assert.strictEqual(result.done, true);
    });
    it('value=undefined', async () => {
      async function* generator() {}
      const result = await generator().next();
      assert.strictEqual(result.value, undefined);
    });
  });
});

describe('does it wait until it has the next data item?', () => {
  it('an async generator with one data item resolves to its value', async () => {
    async function* generator() {
      yield 42;
    }
    const result = await generator().next();
    assert.strictEqual(result.value, 42);
  });
  it('an async generator yield a Promise resolves to the Promises value', async () => {
    async function* generator() {
      yield Promise.resolve(42);
    }
    const result = await generator().next();
    assert.strictEqual(result.value, 42);
  });
  it('an async generator yielding a nested Promise resolves to the inner most Promises value', async () => {
    async function* generator() {
      yield Promise.resolve(Promise.resolve(42));
    }
    const result = await generator().next();
    assert.strictEqual(result.value, 42);
  });
  it('nesting two promises resolves the value of the inner most Promise', async () => {
    const result = await Promise.resolve(Promise.resolve(42));
    assert.strictEqual(result, 42);
  });
  it('nesting where the inner promise is delayed await will wait for all promises to resolve', async () => {
    const delayedPromise = new Promise(
      resolve => setTimeout(() => resolve(42), 1000));
    const result = Promise.resolve(delayedPromise);
    assert.strictEqual(await result, 42);
  });
});

describe('simulate slow data using setTimeout', () => {
  it('yielding a delayed promise inside from an async generator resolve to its value', async () => {
    const delayedPromise = new Promise(resolve => {
      setTimeout(() => resolve(42), 100)
    });
    async function* generator() {
      yield delayedPromise;
    }
    const result = await generator().next();
    assert.strictEqual(result.value, 42);
  });
  it('yielding a slow value before the fast value resolves to the slow value first', async () => {
    const slowValue = new Promise(resolve => {
      setTimeout(() => resolve(42), 500)
    });
    async function* generator() {
      yield slowValue;
      const fastValue = 43;
      yield fastValue;
    }
    const generatorInstance = generator();
    const firstResult = await generatorInstance.next();
    assert.strictEqual(firstResult.value, 42);
  });
});
