# Bitwise operator

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what is the Bitwise Operators
4) [x] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] How can we do multiplication using bitwise operations?
- [ ] How can we fix the 0.0000001 thingy with bitwise opertations?
- [ ] 8 => 0000 1000 ... how can we make this 0 using bitwise?
- [ ] Can we measure diff in performance?
- [ ] How can I manipulate colors (hex) using bitwise operators?





## Topic for the next meetup



 



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
