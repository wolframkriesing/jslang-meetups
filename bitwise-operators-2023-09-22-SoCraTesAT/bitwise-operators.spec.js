import {strict as assert} from 'assert';

describe('How can we do multiplication using bitwise operations?', () => {
  it('if we shift a number to the left by 1 bit, it is a multiplication by 2', () => {
    assert.equal(0b0101 << 1, 0b1010);
  });
  it('if we shift it left by 33 bits THEN the result is left shifted by 1 (33 % 32)', () => {
    assert.equal(1 << 33, 2);
  });
  it('if we shift a number by 32 bits THEN it stays the same', () => {
    assert.equal(1 << 32, 1);
  });
  it('shifting a number by 0.5 will be like shifting it by 0', () => {
    assert.equal(1 << 0.5, 1);
  });
  it('shifting a number by 3.14 is like shifting it by 3', () => {
    assert.equal(1 << Math.PI, 1 << 3);
  });
  it('shifting it by 0.9 will shift it by 0', () => {
    assert.equal(1 << 0.9, 1);
  });

  it('shifting by 3 bits to the left is like multiplaction by 2^3', () => {
    assert.equal(1 << 3, 0b1000);
  });
});