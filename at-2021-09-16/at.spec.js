import {strict as assert} from 'assert';

describe('does `at()` work on strings', () => {
  it('using `at(0)` on "hello" returns "h"', () => {
    assert.equal('hello'.at(0), 'h');
  });
  it('using `at(-1)` on "hello" returns "o", the last character', () => {
    assert.equal('hello'.at(-1), 'o');
  });
  it('passing two values to `at(0, -1)` on "Anna" returns "A", the second argument is ignored', () => {
    assert.equal('Anna'.at(0, -1), 'A');
  });
  it('using at(10) on "hello" returns undefined', () => {
    assert.equal('hello'.at(10), undefined);
  });
  it('using at(-10) on "hello" returns undefined', () => {
    assert.equal('hello'.at(-10), undefined);
  });
});

it('at() is NOT implemented on numbers', () => {
  assert.equal(typeof Number(356).at, 'undefined');
});

it('at() is NOT implemented on objects', () => {
  assert.equal(typeof {}.at, 'undefined');
});

describe('at() in arrays', () => {
  it('at() does NOT access a custom index on an array', () => {
    const arr = ['a', 'b'];
    arr['hello'] = 'c';
    assert.equal(arr.at('hello'), 'a');
  });
  it('at() with a string as parameter "sees" it as 0', () => {
    assert.equal([23, 42].at('1 fish'), 23);
  });
  it('at() with a stringified number uses that numbers as index', () => {
    assert.equal([1, 2].at('1'), 2);
  });
  it('at() with a stringified number and a space after still uses that number', () => {
    assert.equal([3, 4].at('1 '), 4);
  });
  it('at()`s index with a preceeding space also works like a number', () => {
    assert.equal([5, 6].at('  1'), 6);
  });
  it('at(NaN) returns the first element', () => {
    assert.equal([1, 2].at(NaN), 1);
  });
  it('at(Infinity) returns undefined', () => {
    assert.equal([1, 2].at(Infinity), undefined);
  });
  it('at([1]) returns the second element', () => {
    assert.equal([1 ,2].at([1]), 2);
  });
  it('at([2]) returns the third element', () => {
    assert.equal([1 ,2, 3].at([2]), 3);
  });
  it('at([5, 6]) will return the first element', () => {
    assert.equal(['first'].at([5, 6]), 'first');
  });
});