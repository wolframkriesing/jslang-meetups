# at()

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what at() is
4) [x] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn
- [x] works on strings/object/...?
- [x] (negative) out of bounds
- [ ] is it chainable?



## Topic for the next meetup



## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language

## References

- MDN page https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap
- ECMAScript Spec as used in the meetup
  - https://tc39.es/ecma262/#sec-weakmap-constructor

- This source code and other meetup's source https://gitlab.com/wolframkriesing/jslang-meetups