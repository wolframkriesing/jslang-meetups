# Prototype - #jslang 71st Edition

Let's figure out what we can learn about it.

## Agenda

Online: Bring your 🍏 🍕 🥤 from 🏡
Onsite: Thanks to TNG for hosting us 🎉   WE ARE HIRING!!!

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are functions
4) [x] Collect what we want to learn 
5) [x] Drive the code, together
6) [x] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn by writing a test

- [x] Can we write a getter for fn.bind()?
- [ ] Can we write a function if a function is an arrow function (or a regular function)?
- [ ] Can we visualize/access/know the internal slots?
- [x] Can we use `this` inside any function?
- [ ] Is there a limit to its scope/realm?
- [ ] Hoisting
- [ ] Order of execution of nested function
- [ ] Can a dynamic function access the same scope as a regular function?
- [ ] Can i override myself?




## Topic for the next meetup

   strict mode
   `this`
   Reflect
   scopes (e.g. lexical, ...)
   
   
   






## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
