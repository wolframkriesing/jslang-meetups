import {strict as assert} from 'assert';

describe('How can it be stricter (typed)?', () => {
  it('a numeric string can be distinguished to not equal to a number', () => {
    assert.notEqual('1', 1);
  });
  it('a numeric string can be distinguished to not strict equal to a number', () => {
    assert('1' !== 1);
  });
  it('a numeric string can be equal to a number if not strictly compared', () => {
    assert('1' == 1);
  });
  it('using parseInt() on a number it returns the same number', () => {
    assert.equal(1, parseInt(1));
  });
  it('floating point number does strict equal an integer', () => {
    assert.equal(1.0, 1);
  });
  it('the floating point number does NOT strictly equal numeric string integer', () => {
    assert(3.0 == '3');
  });
  it('a leading zero on a number does equal to a numeric string', () => {
    assert(0.3 == '0.3');
  });
  it('prefix a numeric string with a 0 does equal the number without the zero', () => {
    assert('070' == 70);
  });
  // SyntaxError: Octal literals are not allowed in strict mode.
  // it('prefix a number with a 0 equals the numeric string without the zero', () => {
  //   assert(011 == '9');
  // });
});

describe('what can be boolean? truthy, falsy, ...', () => {
  it('the type of a boolean value equals the type of the result of an expression', () => {
    assert.equal(typeof true, typeof (1 !== 1));
  });
  it('an empty string is falsy', () => {
    assert('' == false);
  });
  it('the number -0 is falsy', () => {
    assert(-0 == false);
  });
  it('0 and -0 are NOT strict equal', () => {
    assert.notEqual(0, -0);
  });
});
