# Types

Let's figure out what we can learn about it.

## Agenda

Bring your 🍏 🍕 🥤 from 🏡

1) [x] Short intro about "JavaScript the Language" Meetup, how we run it and what is in and what not.
2) [x] Introduce each other
3) [x] Quickly align on what are types
4) [x] Collect what we want to learn 
5) [ ] Drive the code, together
6) [ ] Choose next meetup topic
7) [ ] Hangout ...

## Collect what we want to learn

- [x] How can it be stricter (typed)?
- [ ] type coercion, how do types change? e.g.: 'string' + 1
- [ ] type conversion, how to cast between types
- [ ] is function a type?
- [ ] what can be boolean? truthy, falsy, ...
- [ ] how to get to a Symbol type?
- [x] Number, what about pure integers

## Topic for the next meetup

- 1 - 0, -0, +0, NaN, -NaN
- 5 - functions
- 2 - overloading
- 8 - arrays




## How we ran the meetup?

We organized the meetup on [meetup].
[meetup]: https://www.meetup.com/JavaScript-The-Language
